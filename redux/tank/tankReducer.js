import {
    CREATE_TANK_REQUEST,
    CREATE_TANK_SUCCESS,
    CREATE_TANK_FAILURE,
    GET_TANKS_REQUEST,
    GET_TANKS_SUCCESS,
    GET_TANKS_FAILURE,
    DELETE_TANKS_REQUEST,
    DELETE_TANKS_SUCCESS,
    DELETE_TANKS_FAILURE,
    UPDATE_TANKS_REQUEST,
    UPDATE_TANKS_SUCCESS,
    UPDATE_TANKS_FAILURE
} from './tankTypes'

const initialState = {
    loading: false,
    tanks: [],
    error: ''
}

const tankReducer = (state = initialState, action) => {
    let { type, payload } = action;
    switch (type) {
        case CREATE_TANK_REQUEST:
            return {
                ...state,
                loading: true
            }
        case CREATE_TANK_SUCCESS:
            return {
                ...state,
                loading: false,
                error: ''
            }
        case CREATE_TANK_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }

        case GET_TANKS_REQUEST:
            return {
                ...state,
                loading: true
            }

        case GET_TANKS_SUCCESS:
            return {
                loading: false,
                tanks: action.payload,
                error: ''
            }
        case GET_TANKS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case DELETE_TANKS_REQUEST:
            return {
                ...state,
                loading: true,
            }
        case DELETE_TANKS_SUCCESS:
            return {
                ...state,
                loading: false,
                error: ''
            }
        case DELETE_TANKS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case UPDATE_TANKS_REQUEST:
            return {
                ...state,
                loading: true,
            }
        case UPDATE_TANKS_SUCCESS:
            return {
                ...state,
                loading: false,
                error: ''
            }
        case UPDATE_TANKS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case 'delete':
            return {
                ...state,
                tanks: state.tanks.filter(x => x.id !== payload.item.id)
            }
        default: return state
    }
}

export default tankReducer