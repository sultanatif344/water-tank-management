import {
    CREATE_TANK_REQUEST,
    CREATE_TANK_SUCCESS,
    CREATE_TANK_FAILURE,
    GET_TANKS_REQUEST,
    GET_TANKS_SUCCESS,
    GET_TANKS_FAILURE,
    DELETE_TANKS_REQUEST,
    DELETE_TANKS_SUCCESS,
    DELETE_TANKS_FAILURE,
}
    from './tankTypes';


export const CreateTankRequest = () => {
    return {
        type: CREATE_TANK_REQUEST
    }
}

export const CreateTankSuccess = () => {
    return {
        type: CREATE_TANK_SUCCESS,
    }
}

export const CreateTankFailure = error => {
    return {
        type: CREATE_TANK_FAILURE,
        payload: error
    }
}

export const GetTankRequest = () => {
    return {
        type: GET_TANKS_REQUEST
    }
}

export const GetTankSuccess = tanks => {
    return {
        type: GET_TANKS_SUCCESS,
        payload: tanks
    }
}

export const GetTankFailure = error => {
    return {
        type: GET_TANKS_FAILURE,
        payload: error
    }
}

export const DeleteTankRequest = () => {
    return {
        type: DELETE_TANKS_REQUEST
    }
}

export const DeleteTankSuccess = () => {
    return {
        type: DELETE_TANKS_SUCCESS
    }
}

export const DeleteTankFailure = error => {
    return {
        type: DELETE_TANKS_FAILURE,
        payload: error
    }
}

export const UpdateTanksRequest = () => {
    return {
        type: UPDATE_TANKS_REQUEST
    }
}

export const UpdateTanksSuccess = () => {
    return {
        type: UPDATE_TANKS_SUCCESS
    }
}

export const UpdateTanksFailure = error => {
    return {
        type: UPDATE_TANKS_FAILURE,
        payload: error
    }
}