import { combineReducers } from 'redux'
import userReducer from './user/userReducer'
import tankReducer from './tank/tankReducer'

const rootReducer = combineReducers({
    user: userReducer,
    tank:tankReducer
})

export default rootReducer