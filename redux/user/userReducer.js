import {
    LOGIN_USER_REQUEST,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    SIGNUP_USER_REQUEST,
    SIGNUP_USER_SUCCESS,
    SIGNUP_USER_FAILURE,
    GET_USERS_REQUEST,
    GET_USERS_SUCCESS,
    GET_USERS_FAILURE,
    DELETE_USER_REQUEST,
    DELETE_USER_SUCCESS,
    DELETE_USER_FAILURE,
    UPDATE_USER_REQUEST,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_FAILURE
} from './userTypes'

const initialState = {
    loading: false,
    users: {},
    allUsers: [],
    error: ''
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_USER_REQUEST:
            return {
                ...state,
                loading: true
            }
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                users: action.payload,
                error: ''
            }
        case LOGIN_USER_FAILURE:
            return {
                ...state,
                loading: false,
                users: {},
                error: action.payload
            }

        case SIGNUP_USER_REQUEST:
            return {
                ...state,
                loading: true
            }

        case SIGNUP_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                error: ''
            }

        case SIGNUP_USER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }

        case GET_USERS_REQUEST:
            return {
                ...state,
                loading: true
            }
        case GET_USERS_SUCCESS:
            return {
                ...state,
                loading: false,
                allUsers: action.payload,
                error: ''
            }
        case GET_USERS_FAILURE:
            return {
                ...state,
                loading: false,
                AllUsers: [],
                error: action.payload
            }
        case DELETE_USER_REQUEST:
            return {
                ...state,
                loading: true
            }
        case DELETE_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                error: ''
            }
        case DELETE_USER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case UPDATE_USER_REQUEST:
            return {
                ...state,
                loading: true,
            }
        case UPDATE_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                error: ''
            }
        case UPDATE_USER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        default: return state
    }
}

export default userReducer