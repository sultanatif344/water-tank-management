import {
    LOGIN_USER_REQUEST,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    SIGNUP_USER_REQUEST,
    SIGNUP_USER_SUCCESS,
    SIGNUP_USER_FAILURE,
    GET_USERS_REQUEST,
    GET_USERS_SUCCESS,
    GET_USERS_FAILURE,
    DELETE_USER_REQUEST,
    DELETE_USER_SUCCESS,
    DELETE_USER_FAILURE,
    UPDATE_USER_REQUEST,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_FAILURE
}
    from './userTypes';


export const LoginUserRequest = () => {
    return {
        type: LOGIN_USER_REQUEST
    }
}

export const LoginUserSuccess = users => {
    return {
        type: LOGIN_USER_SUCCESS,
        payload: users
    }
}

export const LoginUserFailure = error => {
    return {
        type: LOGIN_USER_FAILURE,
        payload: error
    }
}

export const SignUpUserRequest = () => {
    return {
        type: SIGNUP_USER_REQUEST
    }
}

export const SignUpUserSuccess = () => {
    return {
        type: SIGNUP_USER_SUCCESS,
    }
}

export const SignUpUserFailure = error => {
    return {
        type: SIGNUP_USER_FAILURE,
        payload: error
    }
}

export const GetUsersRequest = () => {
    return {
        type: GET_USERS_REQUEST
    }
}

export const GetUsersSuccess = users => {
    return {
        type: GET_USERS_SUCCESS,
        payload: users
    }
}

export const GetUsersFailure = error => {
    return {
        type: GET_USERS_FAILURE,
        payload: error
    }
}

export const DeleteUserRequest = () => {
    return {
        type: DELETE_USER_REQUEST
    }
}

export const DeleteUserSuccess = () => {
    return {
        type: DELETE_USER_SUCCESS
    }
}

export const DeleteUserFailure = error => {
    return {
        type: DELETE_USER_FAILURE,
        payload: error
    }
}

export const UpdateUserRequest = () => {
    return {
        type: UPDATE_USER_REQUEST
    }
}

export const UpdateUserSuccess = () => {
    return {
        type: UPDATE_USER_SUCCESS
    }
}

export const UpdateUserFailure = error => {
    return {
        type: UPDATE_USER_FAILURE,
        payload: error
    }
}