/**
 * @format
 */
import React, { Component } from 'react'
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import messaging from '@react-native-firebase/messaging';
import { onRemoteMessageReceived } from './helpers/fcmHelper';
import notifee ,{EventType} from "@notifee/react-native";

// Register background handler

messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('Message handled in the background!', remoteMessage);
    const authSettings = await notifee.requestPermission({
        alert: true,
        criticalAlert: true,
        announcement: true,
        badge: true,
        carPlay: true,
        provisional: false,
        sound: true,
      });
    onRemoteMessageReceived(remoteMessage);
});

notifee.onBackgroundEvent(async ({ type, detail }) => {
    const { notification, pressAction } = detail;
    console.log("onBackgroundEvent", type, detail);    
    // Check if the user pressed the "Mark as read" action
    if (type === EventType.ACTION_PRESS && pressAction.id === "mark-as-read") {
        console.log("mark-as-read");
        // Remove the notification
        //   if (detail?.notification?.data)
        //     handleNotificationTapped(detail.notification, "onBackgroundEvent");
        await notifee.cancelNotification(notification.id);
    } else if (type === EventType.PRESS) {
        console.log("EventType.PRESS");
        //   if (detail?.notification?.data)
        //     handleNotificationTapped(detail.notification, "onBackgroundEvent");
    }
});
AppRegistry.registerComponent(appName, () => App);
