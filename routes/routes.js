import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { View, StyleSheet } from 'react-native';
import WelcomeScreen from '../screens/Welcome'
import SignIn from '../screens/SignInPage'
import { COLORS } from '../components/theme';
import DashboardScreen from '../screens/Dashboard'
import AddNewTank from '../screens/AddNewTank'
import TankList from '../screens/TankList'
import WaterLevels from '../screens/WaterLevels'
import WaterStatus from '../screens/WaterStatus'
import MotorStatus from '../screens/MotorStatus'
import AdminPanel from '../screens/AdminPanel'
import NewUser from '../screens/NewUser'
import AddCylindricalTankPage from '../screens/AddCylindricalTank'
import AddCubicalTankPage from '../screens/AddCubicalTankPage'
import TabBarNav from '../components/BottomTabNav';
import Userlist from '../screens/Userlist'
import ResetPasswordScreen from '../screens/ResetPassword';
import ReviewsList from '../screens/reviewsList';
import AddReview from '../screens/addreview';
import AboutScreen from '../screens/About';
import FaqScreen from '../screens/faq';
import { Icon } from 'react-native-elements'
import { TouchableOpacity } from 'react-native-gesture-handler';
export const MainRoute = ({ route }) => {
    const MainStack = createStackNavigator();
    return (
        <MainStack.Navigator initialRouteName={route}>
            <MainStack.Screen name="Welcome" component={WelcomeScreen} options={{
                title: ' ',
                headerLeft: () => null,
                headerStyle: {
                    height: 0,
                    shadowColor: 'transparent',
                    borderBottomWidth: 0,
                    elevation: 0,
                }
            }}
            />

            <MainStack.Screen name="SignIn" component={SignIn} options={{
                title: ' ',
                headerTintColor: '#ffffff',
                headerLeft: () => null,
                headerStyle: {
                    height: 0,
                    shadowColor: 'transparent',
                    borderBottomWidth: 0,
                    elevation: 0,
                }
            }} />

            <MainStack.Screen name="Dashboard" component={TabBarNav} options={{
                title: ' ',
                headerLeft: () => null,
                headerStyle: {
                    height: 0,
                    shadowColor: 'transparent',
                    borderBottomWidth: 0,
                    elevation: 0,
                }
            }}
            />

            <MainStack.Screen name="AddNewTank" component={AddNewTank} options={{
                title: 'New Tank',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 100,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }} />
            <MainStack.Screen name="TankList" component={TankList} options={{
                title: 'Tank List',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 100,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84,
                },
            }} />

            <MainStack.Screen name="WaterLevels" component={WaterLevels} options={{
                title: 'Water Levels',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 100,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }} />
            <MainStack.Screen name="WaterStatus" component={WaterStatus} options={{
                title: 'Water Status',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 100,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }} />
            <MainStack.Screen name="MotorStatus" component={MotorStatus} options={{
                title: 'Motor Status',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 100,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }} />
            <MainStack.Screen name="AdminPanel" component={AdminPanel} options={{
                title: 'Admin Panel',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 100,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84,
                },
            }} />

            <MainStack.Screen name="NewUser" component={NewUser} options={{
                title: 'New User',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 80,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }} />

            <MainStack.Screen name="AddCubicalTankPage" component={AddCubicalTankPage} options={{
                title: 'Cubical Tank',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 80,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }}
            />

            <MainStack.Screen name="AddCylindricalTankPage" component={AddCylindricalTankPage} options={{
                title: 'Cylindrical Tank',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 80,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }}
            />

            <MainStack.Screen name="Userlist" component={Userlist} options={{
                title: 'All User',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 80,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }} />

            <MainStack.Screen name="Reset Password" component={ResetPasswordScreen} options={{
                title: 'Reset Password',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 80,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }} />

            <MainStack.Screen name="All Reviews" component={ReviewsList} options={{
                title: 'All Reviews',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 80,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84,
                }
            }} />


            <MainStack.Screen name="Feedback" component={AddReview} options={{
                title: 'Feedback',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 80,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }} />

            <MainStack.Screen name="About" component={AboutScreen} options={{
                title: 'About',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 80,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }} />

            <MainStack.Screen name="Faqs" component={FaqScreen} options={{
                title: 'FAQs',
                headerTintColor: '#ffffff',
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 80,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                }
            }} />
        </MainStack.Navigator>
    )
}


