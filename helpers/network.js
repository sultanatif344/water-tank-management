import { GLOBAL } from '../components/global';
import axios from 'axios';
export const getLoginUser = (credentials) => {
    return new Promise(resolve => {
        console.log("got credentials here", credentials)
        const headers = {
            'Content-Type': 'application/json'
        }
        axios.post(GLOBAL.BASE_URL + 'users/authenticate',
            credentials, {
            headers: headers
        })
            .then(res => {
                if (res) {
                    resolve(res.data);
                }
            }).catch((error) => {
                resolve(error);
            })
    })
}

export const signUpUser = (details) => {
    return new Promise(resolve => {
        const headers = {
            'Content-Type': 'application/json'
        }
        axios.post(GLOBAL.BASE_URL + 'users/register', details, {
            headers: headers
        }).then(res => {
            if (res) {
                resolve(res.data);
            }
        })
    })
}

export const addTank = (tankDetails, token) => {
    return new Promise(resolve => {
        console.log("got token here", { tankDetails })
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
        console.log('authorized header', headers);
        axios.post(GLOBAL.BASE_URL + 'tanks/createTank', tankDetails, {
            headers: headers
        }).then(res => {
            if (res) {
                resolve(res.data);
            }
        })
    })
}


export const getAllTanks = (token) => {
    return new Promise(resolve => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
        axios.get(GLOBAL.BASE_URL + 'tanks/getAllTanks', {
            headers: headers
        }).then(res => {
            if (res.data.tanks) {
                resolve(res.data.tanks);
            }
        }).catch((error) => {
            resolve(error)
        })
    })
}

export const deleteTank = (token, id) => {
    return new Promise(resolve => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
        axios.delete(GLOBAL.BASE_URL + `tanks/deleteTank/${id}`, {
            headers: headers
        }).then(res => {
            resolve(res.data);
        })
    })
}

export const fetchUsers = (token) => {
    return new Promise(resolve => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
        axios.get(GLOBAL.BASE_URL + 'users/getAll', {
            headers: headers
        }).then(res => {
            if (res.data.users.length > 0) {
                resolve(res.data.users);
            }
        })
    })
}

export const deleteUser = (token, id) => {
    return new Promise(async resolve => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }

        axios.delete(GLOBAL.BASE_URL + `users/deleteUser/${id}`, {
            headers: headers
        }).then(() => {
            resolve(true);
        })
    })
}

export const updateUser = (token, id, details) => {
    return new Promise(resolve => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
        console.log("update details", { details })
        axios.put(GLOBAL.BASE_URL + `users/updateUser/${id}`, details, {
            headers: headers
        }).then(res => {
            resolve(res);
        })
    })
}

export const updateTank = (token, id, details) => {
    return new Promise(resolve => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
        axios.put(GLOBAL.BASE_URL + `tanks/updateTank/${id}`, details, {
            headers: headers
        }).then(res => {
            resolve(res.data);
        })
    })
}

export const updateNotificationToken = (user, fcmToken) => {
    return new Promise(resolve => {
        console.log("user here", user)
        const local = user;
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${local.token}`
        }
        console.log("running", headers);
        axios.put(GLOBAL.BASE_URL + `users/updateNotificationToken/${local._id}`, { "notificationToken": fcmToken }, {
            headers: headers
        }).then(res => {
            console.log("this is res", res.data);
            resolve(res)
        }).catch((error) => {
            console.log("this is error", error);
            resolve(error)
        })
    })
}

export const resetPassword = (token, id, oldPassword, newPassword) => {
    return new Promise((resolve, reject) => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
        console.log("running", headers);
        axios.put(GLOBAL.BASE_URL + `users/resetPassword/${id}`, { "oldPassword": oldPassword, "newPassword": newPassword }, {
            headers: headers
        }).then(res => {
            console.log("this is res", res.data);
            resolve(res.data)
        }).catch((error) => {
            console.log("this is error", error);
            resolve(error)
        })
    })
}

export const requestToggleTank = (token, previousToggledTankActivationId, tankToToggleActivationId, toggleValue) => {
    return new Promise((resolve, reject) => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
        console.log("running", headers);
        axios.post(GLOBAL.BASE_URL + 'tanks/toggleTank', { "previousToggledTankActivationId": previousToggledTankActivationId, "tankToToggleActivationId": tankToToggleActivationId, "toggleValue": toggleValue }, {
            headers: headers
        }).then(res => {
            console.log("this is res", res.data);
            resolve(res.data)
        }).catch((error) => {
            console.log("this is error", error);
            reject(error)
        })
    })
}

export const sendCustomNotification = (token, body) => {
    return new Promise((resolve, reject) => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }

        axios.post(GLOBAL.BASE_URL + 'tanks/triggerTankStatusNotification', { 'payload': body }, {
            headers: headers
        }).then(res => {
            resolve(res.data)
        }).catch((error) => {
            reject(error)
        })
    })
}

export const storeNotificationHistory = (token, body) => {
    return new Promise((resolve, reject) => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }

        axios.post(GLOBAL.BASE_URL + 'users/saveNotificationHistory', body, {
            headers: headers
        }).then(res => {
            resolve(res)
        }).catch((error) => {
            reject(error)
        })
    })
}

export const getNotificationHistory = (token) => {
    return new Promise((resolve, reject) => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }

        axios.get(GLOBAL.BASE_URL + 'users/getAllNotifications', {
            headers: headers
        }).then(res => {
            resolve(res)
        }).catch((error) => {
            reject(error)
        })
    })
}

export const deleteNotificationHistory = (token, id) => {
    return new Promise((resolve, reject) => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }

        axios.delete(GLOBAL.BASE_URL + `users/deleteNotificationHistory/${id}`, body, {
            headers: headers
        }).then(res => {
            resolve(res)
        }).catch((error) => {
            reject(error)
        })
    })
}

export const addUserReview = (token, review) => {
    return new Promise((resolve, reject) => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }

        axios.post(GLOBAL.BASE_URL + `users/AddUserReview`, review, {
            headers: headers
        }).then(res => {
            resolve(res)
        }).catch((error) => {
            reject(error)
        })
    })
}

export const getAllReviews = (token) => {
    return new Promise((resolve, reject) => {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }

        axios.get(GLOBAL.BASE_URL + `users/getAllReviews`, {
            headers: headers
        }).then(res => {
            resolve(res)
        }).catch((error) => {
            reject(error)
        })
    })
}