import AsyncStorage from "@react-native-community/async-storage";
import Toast from 'react-native-toast-message';
export const setItemLocally = (item, key) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.setItem(key, item, (error, result) => {
            console.log(item);
            if (error) {
                reject(error)
            }
            else {
                console.log("got result", result)
                resolve(result);
            }
        });
    })
}

export const getLocallyStoredItem = (key) => {
    return new Promise((resolve) => {
        AsyncStorage.getItem(key, (error, result) => {
            if (error) {
                resolve(error)
            }
            else {
                console.log("get result", result)
                resolve(result)
            }
        })
    })
}

export const presentSuccessToast = (message) => {
    Toast.show({
        type: 'success',
        text1: message
    })
}

export const presentFailureToast = (message) => {
    Toast.show({
        type: 'error',
        text1: message
    })
}

export const removeLocallyStoredItem = (key) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.removeItem(key, (error) => {
            reject(error);
        })
        resolve(true);
    })
}

export const clearAllLocalItems = () => {
    AsyncStorage.clear();
}
