import { getLoginUser, signUpUser, fetchUsers, deleteUser, updateUser, updateNotificationToken, resetPassword, storeNotificationHistory, getNotificationHistory, addUserReview, getAllReviews } from '../helpers/network';
import AsyncStorage from '@react-native-community/async-storage';
export function storeToken(token) {
    localStorage.setItem('token', token);
}

export function getToken() {
    const token = localStorage.getItem('token');

    return token;
}


export function storeUserDetails(userDetails) {
    localStorage.setItem('userdetails', userDetails);
}


export function getUserDetails() {
    const userDetails = localStorage.getItem('userdetails');

    return userDetails;
}



export const authenticateUser = (credentials) => {
    return new Promise(async resolve => {
        console.log(credentials);
        const res = await getLoginUser(credentials);
        console.log("my response", res)
        resolve(res);
    })
}

export const signUp = (details) => {
    console.log("details here", details);
    return new Promise(async resolve => {
        const res = await signUpUser(details);
        resolve(res);
    })
}

export const updateFcmToken = (user, fcmToken) => {
    return new Promise(async resolve => {
        console.log("got fcmtoken", fcmToken)
        console.log("got user", user)
        const res = await updateNotificationToken(user, fcmToken);
        resolve(res);
    })
}

export const setUserLocally = (user) => {
    return new Promise((resolve, reject) => {
        const userDetail = JSON.stringify(user);
        AsyncStorage.setItem("user", userDetail, (error, result) => {
            if (error) {
                reject(error)
            }
            else {
                resolve(result);
            }
        });
    })
}

export const getLocallyStoredUser = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem("user", (error, result) => {
            if (error) {
                resolve(error)
            }
            else {
                console.log("here is user", result)
                resolve(result)
            }
        })
    })
}


export const getAllUsers = (token) => {
    return new Promise(async resolve => {
        const res = await fetchUsers(token);
        resolve(res);
    })
}

export const deleteUserById = (token, id) => {
    return new Promise(async resolve => {
        const res = await deleteUser(token, id);
        resolve(res);
    })
}

export const updateUserById = (token, id, details) => {
    return new Promise(async resolve => {
        const res = await updateUser(token, id, details);
        resolve(res.data);
    })
}

export const requestToResetPassword = (token, id, oldPassword, newPassword) => {
    return new Promise(async (resolve) => {
        const res = await resetPassword(token, id, oldPassword, newPassword);
        resolve(res);
    })
}

export const addNotificationHistory = (token, body) =>{
    return new Promise(async (resolve)=>{
        const res = await storeNotificationHistory(token, body);
        resolve(res); 
    })
}

export const getAllNotifications = (token) =>{
    return new Promise(async resolve =>{
        console.log("notification history token",token);
        const res = await getNotificationHistory(token)
        resolve(res.data.response);
    })
}

export const sendReviewDetails = (token,review)=>{
    return new Promise(async resolve =>{
        const res = await addUserReview(token, review);
        resolve(res.data);
    })
}

export const requestAllReviews = (token)=>{
    return new Promise(async resolve =>{
        const res = await getAllReviews(token);
        resolve(res.data.response);
    })
}
