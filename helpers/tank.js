import { addTank, getAllTanks, deleteTank, updateTank, requestToggleTank, sendCustomNotification } from '../helpers/network';
import { getDataOnAdd, getDataOnce } from './firebase';
import { getLocallyStoredUser } from './user';
import { getLocallyStoredItem } from './utility';


export const addTankDetails = (tankDetails, token) => {
    return new Promise(async resolve => {
        const res = await addTank(tankDetails, token);
        resolve(res);
    })
}

export const getTanks = (token) => {
    return new Promise(async resolve => {
        const res = await getAllTanks(token);
        console.log("here are the tanks", res)
        resolve(res);
    })
}

export const deleteTankById = (token, id) => {
    return new Promise(async resolve => {
        const res = await deleteTank(token, id);
        resolve(true);
    })
}

export const updateTankById = (token, id, details) => {
    return new Promise(async resolve => {
        const res = await updateTank(token, id, details);
        resolve(res);
    })
}

export const sendPayloadToToggleTank = (token, previousToggledTankActivationId, tankToToggleActivationId, toggleValue) => {
    return new Promise(async resolve => {
        const res = await requestToggleTank(token, previousToggledTankActivationId, tankToToggleActivationId, toggleValue);
        resolve(res);
    })
}


export const getActivatedTankFromLocal = () => {
    return new Promise(async (resolve, reject) => {
        getLocallyStoredItem('activatedTank').then(res => {
            console.log("here is my tank", res)
            resolve(res);
        })
    })
}

export const getTankFilledValueInPercentage = (height) => {
    return new Promise(async (resolve, reject) => {
        let date = new Date().getTime();
        const firebaseRes = await getDataOnAdd(date);
        console.log(firebaseRes);
        let distancePercentage = ((height - firebaseRes[0]) / height) * 100;
        resolve(distancePercentage);
    }).catch((error) => {
        alert(error);
        reject(error);
    })
}


export const getAllActiveTanksList = () => {
    return new Promise(async resolve => {
        const user = JSON.parse(await getLocallyStoredUser());
        if (user != null) {
            var tanksList = await getTanks(user.token);
            tanksList = tanksList.filter((element) => element.activated == true);
            if (tanksList.length > 0) {
                console.log("Tankslist", tanksList);
                resolve(tanksList[0])
                // setActiveTank(tanksList[0]);
            }
            else{
                resolve([])
            }
        }
    })
}


export const sendCustomNotificationPayload = (body) => {
    return new Promise(async (resolve, reject) => {
        console.log("notification body", body)
        const payload = {
            'body': body.body,
            'title': body.title,
            'fcmToken': body.fcmToken
        }

        const user = JSON.parse(await getLocallyStoredUser());

        if (user != null) {
            sendCustomNotification(user.token, payload).then((res => {
                resolve(res);
            }))
                .catch((error) => {
                    reject(error)
                })
        }
    })
}

export const getPreviouslyActiveTankFromLocal = () => {
    return new Promise((resolve) => {
        getLocallyStoredItem('previouslyactiveTank').then(res => {
            console.log("here is my tank", res)
            resolve(res);
        })
    })
}
// export const getActivatedTank = async () => {
//     const res = await getActivatedTankFromLocal();
//     return res
// }
