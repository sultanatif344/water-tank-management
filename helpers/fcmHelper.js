import notifee from "@notifee/react-native";

const createNotificationChannel = () => {
    // Create a channel
    return notifee.createChannel({
        id: "default",
        name: "Default Channel",
    });
};

export const onRemoteMessageReceived = (remoteMessage) => {
    console.log("onRemoteMessageReceived", JSON.stringify(remoteMessage));
    let notification = remoteMessage.notification;
    let data = remoteMessage.data;
    // getNotifications();
    if (notification)
        onDisplayNotification(notification.title, notification.body, data);
    else if (data) onDisplayNotification(data.title, data.body, data);
};

async function onDisplayNotification(title, body, data) {
    let channelId = await createNotificationChannel();
    console.log("channelId -> ", channelId);
    // Display a notification
    let s = await notifee.displayNotification({
        title,
        body,
        android: {
            channelId,
            pressAction: {
                id: "default",
            },
            smallIcon: "bell", // optional, defaults to 'ic_launcher'.
        },
        data,
    });
    console.log(s, "onDisplayNotification");
}

// export const handleNotificationTapped = async (notification, from) => {
//     console.log("handleNotificationTapped ", from, notification);
//     let user = await getUser();

//     let referenceId = notification?.data?.ReferenceID?.toString();
//     let NotificationId = notification?.data?.NotificationId;
//     let type = notification?.data?.NotificationType?.toString();

//     if (!user || !referenceId) return;

//     if (type === NotificationType.Message.toString())
//         navigate("chat", {
//             complaint: {
//                 id: referenceId,
//             },
//         });
//     else getComplaint(referenceId);

//     onReadNotification(NotificationId);

//     // if (type === NotificationType.FILE_RECEIVED) {
//     //   console.log("ReceiveFiles");
//     //   navigate("ReceiveFiles");
//     // } else if (
//     //   type === NotificationType.I_ACCEPTED_FILE ||
//     //   type === NotificationType.FILE_PENDING
//     // ) {
//     //   console.log("MyFiles");
//     //   navigate("MyFiles");
//     // } else if (type === NotificationType.FILE_ACCEPTED) {
//     //   console.log("History");
//     //   navigate("History");
//     // } else {
//     //   navigate("MyFiles");
//     //   console.log("MyFiles");
//     // }
// };
