import { firebase } from '@react-native-firebase/database';


const reference = firebase.app().database("https://fyp-test-rapier909-default-rtdb.firebaseio.com/").ref("data_table");

export const getDataOnce = (recentDate) => {
    return new Promise((resolve, reject) => {

        // reference.once('value').then(snapshot => {
        //     console.log(snapshot.val());
        // })
        reference.orderByChild("timestamp").limitToLast(1).once('value').then(snapshot => {
            console.log("heresnapshot", snapshot.val());
            if (snapshot.val() != null) {
                let id = Object.keys(snapshot.val())
                resolve(snapshot.child(...id).val())
            }
            else {
                resolve(false);
            }
        }, error => {
            alert(error);
            reject(error)
        })
    })
}

export const getDataOnAdd = (recentDate) => {
    return new Promise((resolve, reject) => {
        reference.orderByChild("timestamp").limitToLast(1).on('value', snapshot => {
            if (snapshot.val() != null) {
                let id = Object.keys(snapshot.val())
                resolve(snapshot.child(...id).val())
            }
            else{
                resolve(false)
            }
        }, error => {
            reject(error)
        })
    })
}

export const stopListeningForUpdatesChildAdded = (firebaseEvent) => {
    return reference.off('child_added', firebaseEvent);
}

export const stopListeningForUpdatesChildRemoved = (firebaseEvent) => {
    return reference.off('child_removed', firebaseEvent);
}

export const updateMotorStatus = (status)=>{
    reference.update({
        motor: status,
    })
}