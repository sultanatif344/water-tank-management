import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { COLORS } from '../components/theme';
import { Text } from 'react-native-elements';
const AddNewTank = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('AddCylindricalTankPage', {
                item: [],
                isEdit: false
            })}>
                <Image source={require('../assets/Images/cylindericalTank.png')} width="60" height="60" />
                <Text style={styles.btnText}>Cylindrical</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('AddCubicalTankPage', {
                item: [],
                isEdit: false
            })}>
                <Image source={require('../assets/Images/cubicleTank.png')} width="60" height="60" />
                <Text style={styles.btnText}>Cubicle</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center'
    },

    btn: {
        marginTop: 40,
        width: 250,
        height: 250,
        backgroundColor: COLORS.Cyan,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: COLORS.Black,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius:20,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 1.25,
        shadowRadius: 10.84,
        elevation: 5
    },


    btnText: {
        marginTop: 20,
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold'
    }
})
export default AddNewTank