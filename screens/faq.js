import React, { Component } from 'react'
import {
    AppRegistry,
    StyleSheet,
    View,
} from 'react-native';
import { Text, Card } from 'react-native-elements';
import { opacity } from 'react-native-reanimated/src/reanimated2/Colors';
import { COLORS } from '../components/theme';

const FaqScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Card containerStyle={styles.card}>
                <Text style={styles.textStyle}>
                   Q1- Can we use the mobile app only at home?
                </Text>
                <Text style={{marginTop:5,...styles.textStyle}}>
                   Ans- No, we can use mobile app at any location (office, park, etc.) because we are using cloud based technology to communicate the mobile app with server this allow user
                    to access its tank or water information from anywhere.
                </Text>
                <Text style={{marginTop:20,...styles.textStyle}}>
                   Q2- Does the user require sign in?
                </Text>
                <Text style={{marginTop:5,...styles.textStyle}}>
                   Ans- Yes the user requires sign in for the authentication to use mobile app.
                </Text>
                <Text style={{marginTop:20,...styles.textStyle}}>
                   Q3- Can user recieve alert and notification about water levels?
                </Text>
                <Text style={{marginTop:5,...styles.textStyle}}>
                   Ans- Yes the user will notify by notification about water levels (25%, 50%, 75% or 100%) for the notification it is necessary the application should be open on background.
                </Text>
            </Card>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    card: {
        width: 380,
        height: 500,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: COLORS.Black,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 1.25,
        shadowRadius: 10.84,
        elevation: 5,
        padding:5
    },

    btn: {
        marginTop: 30,
        width: 162,
        height: 50,
        backgroundColor: COLORS.Cyan,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
    },
    checkBoxText: {
        color: COLORS.White,
        marginLeft: 10,
        fontSize: 12,
    },
    iconMargin: {
        marginBottom: 5,
    },
    radioStyle: {
        marginLeft: 100,
        marginTop: 20,
    },
    textInputCustom: {
        fontSize: 12,
        borderColor: COLORS.White,
        borderBottomWidth: 1,
        width: 350,
        marginLeft: 5,
        marginTop: 15,
        color: COLORS.White,
    },

    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    wrapper: {
        display: 'flex',
        marginTop: 20,
        marginRight: 10,
    },

    secondwrapper: {
        display: 'flex',
        marginTop: 5,
        flexDirection: 'row',
    },

    containerwrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },

    textStyle:{
        fontSize:12,
        color:COLORS.White,
        fontWeight: 'bold',
        padding:5,
        marginLeft:12
    }
});

export default FaqScreen;