// import React from 'react'
// import { View, Text } from 'react-native'
// import UserListTvc from '../components/userlisttvc';
// const Userlist = () => {
//     return (
//         <View>

//         </View>
//     )
// }

// export default Userlist


import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList, Text, TouchableOpacity } from 'react-native';
import { getAllUsers } from '../helpers/user'
import { useSelector, useDispatch } from 'react-redux';
import { getLocallyStoredUser } from '../helpers/user';
import { getTanks, deleteTankById } from '../helpers/tank'
import { GetTankRequest, GetTankSuccess, GetTankFailure, DeleteTankRequest, DeleteTankSuccess, DeleteTankFailure } from '../redux/tank/tankActions';
import { ListItem } from 'react-native-elements/dist/list/ListItem';
import { COLORS } from '../components/theme';
import { Card, Icon } from 'react-native-elements';
import { DeleteUserRequest, DeleteUserSuccess, DeleteUserFailure } from '../redux/user/userActions';
import { deleteUserById } from '../helpers/user';
import { presentSuccessToast } from '../helpers/utility';
import Toast from 'react-native-toast-message';
const Userlist = ({ navigation }) => {

    // useEffect(() => {

    // },[navigation])

    const tankSelector = useSelector((state) => state.tank);
    const userSelector = useSelector((state) => state.user);
    const [refresh, setRefresh] = useState(false);
    const [localUser, setLocalUser] = useState({});
    const [userList, setUserList] = useState([]);
    const dispatch = useDispatch();
    const getAllUserslist = async () => {
        const storedUser = JSON.parse(await getLocallyStoredUser());
        const token = storedUser.token;
        setLocalUser(storedUser);
        const userslist = await getAllUsers(token);
        if (userslist.length > 0) {
            console.log("users list", userslist);
            setUserList(userslist)

        }
    }

    const deleteUser = async (token, id) => {
        const res = await deleteUserById(token, id);
        const listItemIndex = userList.findIndex((e) => e._id == id)
        setUserList((prevState) => {
            const removed = prevState.splice(listItemIndex, 1);
            presentSuccessToast("User Deleted!")
            return [...prevState];
        })
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getAllUserslist();
        })
        return unsubscribe;
    }, [navigation]);

    const onRefresh = () => {
        setRefresh(true);
        getAllUserslist(localUser.token).then(() => {
            setRefresh(false);
        })
    }
    return (
        <View>
            <View style={{ position: 'relative', zIndex: 10 }}>
                <Toast />
            </View>
            <View>
                <FlatList
                    data={userList}
                    renderItem={({ item }) =>
                    (
                        <View style={styles.container}>
                            <Card containerStyle={styles.card}>
                                <View style={styles.containerwrapper}>
                                    <View style={styles.wrapper}>
                                        <Text style={styles.BoxText}>{item.username}</Text>

                                        <Text style={styles.BoxText}>{item.email}</Text>

                                        <Text style={styles.BoxText}>{item.userType}</Text>
                                    </View>
                                    <View style={styles.wrapper}>
                                        <View style={styles.secondwrapper}>
                                            <TouchableOpacity onPress={() => navigation.navigate("NewUser", {
                                                item: item,
                                                isEdit: true
                                            })}><Text style={styles.deleteStyle}><Icon name='edit'
                                                type='font-awesome'
                                                color={COLORS.White}
                                                size={20}
                                            />   Edit</Text></TouchableOpacity>
                                        </View>
                                        <View style={styles.secondwrapper}>
                                            <TouchableOpacity onPress={() => deleteUser(localUser.token, item._id)}><Text style={styles.deleteStyle}><Icon name='trash'
                                                type='font-awesome'
                                                color={COLORS.White}
                                                size={20}
                                            />   Delete</Text></TouchableOpacity>
                                        </View>
                                    </View>

                                    {/* <View style={styles.secondwrapper}>
                                <TouchableOpacity onPress={()=>navigation.navigate("NewUser",{
                                    item:user,
                                    isEdit:true
                                })}>
                                    <Text style={styles.editStyle}>Edit User</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.secondwrapper}>
                                <TouchableOpacity onPress={deleteUser(userSelector.users.token,user._id)}><Text style={styles.deleteStyle}>Delete Tank</Text></TouchableOpacity>
                            </View> */}
                                </View>
                            </Card >
                        </View >

                    )
                    }
                    onRefresh={onRefresh}
                    refreshing={refresh}
                    progressViewOffset={100}
                    extraData={userList}
                    keyExtractor={(item, index) => item._id}
                />
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        fontSize: 20,
        color: COLORS.White
    },
    editStyle: {
        fontSize: 13,
        color: COLORS.White,
        width: 130,
        marginTop: 5,
        marginLeft: 25,
        marginBottom: 5,
        height: 40,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 100,
        paddingTop: 5,
        paddingBottom: 10,
        textAlign: 'center'
    },
    deleteStyle: {
        fontSize: 13,
        color: COLORS.White,
        width: 130,
        marginTop: 5,
        marginLeft: 25,
        marginBottom: 5,
        height: 40,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 100,
        paddingTop: 5,
        paddingLeft: 15,
        paddingBottom: 10,
        textAlign: 'center'
    },
    card: {
        flexDirection: 'row-reverse',
        flexWrap: 'nowrap',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: COLORS.Black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 2,
        shadowRadius: 10.84,
        elevation: 5,
        backgroundColor: COLORS.Cyan,
        paddingVertical: 15,
        width: 'auto'
    },
    BoxText: {
        color: COLORS.White,
        marginTop: 5,
        marginLeft: 10,
        fontSize: 12
    },

    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    wrapper: {
        display: 'flex',
        marginRight: 20,
        width: 150
    },

    secondwrapper: {
        display: 'flex',
        flexDirection: 'row',
    },

    containerwrapper: {
        display: 'flex',
        flexDirection: 'row',
    }
})

export default Userlist