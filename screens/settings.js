import React from 'react';
import { View, TouchableOpacity, StyleSheet } from "react-native"
import { Card, Text } from 'react-native-elements';
import { COLORS } from '../components/theme';
import { Icon } from 'react-native-elements';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';



const Settings = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <TouchableWithoutFeedback onPress={()=>navigation.navigate("Faqs")} style={styles.clickableStyle}>
                <View style={styles.inline}>
                        <Text style={styles.textColor}>
                            FAQ
                        </Text>
                    {/* <View style={styles.secondwrapper}>
                        <Icon name="chevron-right" type='font-awesome' size={20} color={COLORS.White} style={{ marginLeft: 50 }} />
                    </View> */}
                </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback style={styles.clickableStyle} onPress={()=>navigation.navigate("About")}>
                <View style={styles.inline}>
                    <View>
                        <Text style={styles.textColor}>
                            About
                        </Text>
                    </View>
                    {/* <View style={styles.reviewsWrapper}>
                        <Icon name="chevron-right" type='font-awesome' size={20} color={COLORS.White} style={{ marginLeft: 50 }} />
                    </View> */}
                </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback style={styles.clickableStyle} onPress={()=> navigation.navigate('All Reviews')}>
                <View style={styles.inline}>
                    <View>
                        <Text style={styles.textColor}>
                            Reviews
                        </Text>
                    </View>
                    {/* <View style={styles.reviewPasswordwrapper}>
                        <Icon name="chevron-right" type='font-awesome' size={20} color={COLORS.White} style={{ marginLeft: 50 }} />
                    </View> */}
                </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback style={styles.clickableStyle} onPress={() => navigation.navigate('Reset Password')}>
                <View style={styles.inline}>
                    <View>
                        <Text style={styles.textColor}>
                            Reset Password
                        </Text>
                    </View>
                    {/* <View style={styles.resetPasswordwrapper}>
                        <Icon name="chevron-right" type='font-awesome' size={20} color={COLORS.White} style={{ marginLeft: 50 }} />
                    </View> */}
                </View>
            </TouchableWithoutFeedback>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center', 
        alignItems:'center',
        marginBottom:70
    },
    clickableStyle: {
        backgroundColor: COLORS.DeepBlue,
        width:350,
        marginLeft: 5,
        padding: 25,
        textAlign:'center',
        height: 90,
        marginTop: 10,
        borderRadius: 10,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    textColor: {
        color: COLORS.White,
        fontSize: 20,
        fontWeight: 'bold',
        textAlign:'center',
    },
    inline: {
        flex: 1,
        flexDirection: 'row',
        justifyContent:'center'
    },
    secondwrapper: {
        marginTop: 5,
        marginLeft: 255
    },
    reviewsWrapper: {
        marginTop: 5,
        marginLeft: 245
    },
    reviewPasswordwrapper: {
        marginTop: 5,
        marginLeft: 230
    },
    resetPasswordwrapper: {
        marginTop: 5,
        marginLeft: 180
    }
})
export default Settings