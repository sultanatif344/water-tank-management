import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, Text } from 'react-native';
import { Card, Icon } from 'react-native-elements'
import { deleteTankById, getActivatedTankFromLocal, getTanks, sendPayloadToToggleTank } from '../helpers/tank'
import { useSelector, useDispatch } from 'react-redux';
import { GetTankRequest, GetTankSuccess, GetTankFailure } from '../redux/tank/tankActions';
import TankListTvc from '../components/tanklisttvc';
import { getLocallyStoredUser } from '../helpers/user';
import { presentFailureToast, presentSuccessToast, removeLocallyStoredItem, setItemLocally } from '../helpers/utility';
import { COLORS } from '../components/theme';
import Toast from 'react-native-toast-message';
import { getLocallyStoredItem } from '../helpers/utility';
const TankList = ({ navigation }) => {
    const tankSelector = useSelector((state) => state.tank);
    const userSelector = useSelector((state) => state.user);
    const [localUser, setLocalUser] = useState({});
    const [previouslyActivatedTank, setPreviouslyActivatedTank] = useState(null);
    const [previousTank, setPreviousActivatedTank] = useState(null);
    console.log("selector", userSelector);
    const dispatch = useDispatch();
    const [refresh, setRefresh] = useState(false);
    const [tanksList, setTanksList] = useState([]);
    const [currentlyActiveTank, setCurrentlyActiveTank] = useState(null);

    const getCurrentAndPreviousTank = async () => {
        setCurrentlyActiveTank(JSON.parse(await getLocallyStoredItem("activatedTank")))
        setPreviouslyActivatedTank(JSON.parse(await getLocallyStoredItem("previouslyActiveTank")))
    }

    const deleteTank = async (id) => {
        const res = await deleteTankById(localUser.token, id);
        if (res == true) {
            const listItemIndex = tanksList.findIndex((e) => e._id == id)
            setTanksList((prevState) => {
                const removed = prevState.splice(listItemIndex, 1);
                presentSuccessToast("Tank Deleted!")
                return [...prevState];
            })
        }
    }

    const toggleTankActive = async (previouslyAddedTankId, tank, tankId, toggleValue) => {
        const previousTankId = previouslyAddedTankId != null ? previouslyAddedTankId : null;
        const res = await sendPayloadToToggleTank(localUser.token, previousTankId, tankId, toggleValue);

        if (res.success == true) {
            const fetchedTanks = await getTanks(localUser.token);
            if (toggleValue == true) {
                const activatedCurrent = await setItemLocally(JSON.stringify(tank), 'activatedTank');
                const activatedPrevious = await setItemLocally(JSON.stringify(tank), 'previouslyActiveTank');
                getCurrentAndPreviousTank();
                setTanksList((prevState) => {
                    presentSuccessToast("Tank Activated")
                    prevState = fetchedTanks;
                    return [...prevState];
                })
            }
            else {
                removeLocallyStoredItem('activatedTank')
                setTanksList((prevState) => {
                    presentSuccessToast("Tank Deactivated")
                    prevState = fetchedTanks;
                    return [...prevState];
                })
            }
        }
    }
    const getAllAddedTanks = async (token) => {
        dispatch(GetTankRequest);
        const fetchedTanks = await getTanks(token);
        if (fetchedTanks.length > 0) {
            dispatch(GetTankSuccess(fetchedTanks));
            setTanksList(fetchedTanks);
        }
        else {
            dispatch(GetTankFailure('Could not fetch'))
        }
    }

    const userSetter = async () => {
        const user = await getLocallyStoredUser();
        const locallyStoredUser = {};
        if (user != null) {
            console.log("this is the local", user)
            const storedUser = JSON.parse(user);
            console.log("here is the stored user", storedUser)
            setLocalUser(storedUser);
            getAllAddedTanks(storedUser.token);
        }
    }

    const getActiveTank = async () => {
        getCurrentAndPreviousTank();
        const activatedTank = tanksList.filter((tank) => tank.activated == true);
        if (activatedTank != undefined) {
            // console.log(activatedTank[0]);
            // setItemLocally(JSON.stringify(activatedTank[0]), 'previouslyactiveTank');
            // setItemLocally(JSON.stringify(activatedTank[0]), 'currentlyActiveTank');
            // const previousActiveTank = await JSON.parse(getActivatedTankFromLocal())
            setPreviouslyActivatedTank(activatedTank[0]);
        }
    }


    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            // The screen is focused
            // Call any action

            userSetter();
            getActiveTank();
        });

        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);

    const onRefresh = () => {
        setRefresh(true);
        userSetter();
        getActiveTank()
        setRefresh(false);
    }
    return (
        <View>
            <View style={{ position: 'relative', zIndex: 10 }}>
                <Toast />
            </View>
            <View>
                <FlatList
                    data={tanksList}
                    renderItem={({ item }) =>
                    (
                        <View style={styles.container}>
                            <Card containerStyle={styles.card}>
                                <View style={styles.containerwrapper}>
                                    <View style={styles.wrapper}>
                                        <View style={styles.secondwrapper}>
                                            <Text style={styles.BoxText}>Shape: {item?.tankShape}</Text>
                                        </View>
                                        {
                                            item?.tankShape == 'Cylinderical' ? (
                                                <View style={styles.secondwrapper}>
                                                    <Text style={styles.BoxText}>Diameter: {item?.diameter}</Text>
                                                </View>
                                            ) : (
                                                <View>
                                                    < View style={styles.secondwrapper}>
                                                        <Text style={styles.BoxText}>Length: {item?.length}</Text>
                                                    </View>

                                                    <View style={styles.secondwrapper}>
                                                        <Text style={styles.BoxText}>Width: {item.width}</Text>
                                                    </View>
                                                </View>
                                            )}
                                        <View style={styles.secondwrapper}>
                                            <Text style={styles.BoxText}>Height: {item?.height}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.wrapper}>
                                        <View style={styles.secondwrapper}>
                                            <TouchableOpacity onPress={() => item.tankShape == "Cubical" ? navigation.navigate("AddCubicalTankPage", {
                                                item: item,
                                                isEdit: true
                                            }) : navigation.navigate("AddCylindricalTankPage", {
                                                item: item,
                                                isEdit: true
                                            })}>
                                                <Text style={styles.editStyle}>
                                                    <Icon name='edit'
                                                        type='font-awesome'
                                                        color={COLORS.White}
                                                        size={20}
                                                    />  Edit
                                                </Text>
                                            </TouchableOpacity>
                                        </View>

                                        <View style={styles.secondwrapper}>
                                            <TouchableOpacity onPress={() => deleteTank(item._id)}><Text style={styles.deleteStyle}><Icon name='trash'
                                                type='font-awesome'
                                                color={COLORS.White}
                                                size={20}
                                            />   Delete</Text></TouchableOpacity>
                                        </View>

                                        <View style={styles.secondwrapper}>
                                            <TouchableOpacity onPress={() => item.activated == false ? toggleTankActive(previouslyActivatedTank ? previouslyActivatedTank?._id : null, item, item._id, true) : toggleTankActive(previouslyActivatedTank ? previouslyActivatedTank?._id : null, item, item._id, false)}>
                                                {
                                                    item.activated == false ?
                                                        <Text style={styles.deleteStyle}><Icon name='power-off'
                                                            type='font-awesome'
                                                            color={COLORS.White}
                                                            size={20}
                                                        />   Activate</Text>
                                                        :
                                                        <Text style={styles.deleteStyle}><Icon name='power-off'
                                                            type='font-awesome'
                                                            color={COLORS.White}
                                                            size={20}
                                                        />   Deactivate</Text>
                                                }
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </Card>
                        </View>
                    )
                    }
                    onRefresh={onRefresh}
                    refreshing={refresh}
                    extraData={tanksList}
                    keyExtractor={(i, index) => i._id}
                />
            </View >
        </View >
    )

}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        fontSize: 20,
        color: COLORS.White
    },
    editStyle: {
        fontSize: 13,
        color: COLORS.White,
        width: 130,
        marginTop: 5,
        marginLeft: 25,
        marginBottom: 5,
        height: 40,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 100,
        paddingTop: 5,
        paddingBottom: 10,
        textAlign: 'center'
    },
    deleteStyle: {
        fontSize: 13,
        color: COLORS.White,
        width: 130,
        marginTop: 5,
        marginLeft: 25,
        marginBottom: 5,
        height: 40,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 100,
        paddingTop: 5,
        paddingLeft: 15,
        paddingBottom: 10,
        textAlign: 'center'
    },
    card: {
        flexDirection: 'row',
        flexWrap: 'nowrap',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: COLORS.Black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 2,
        shadowRadius: 10.84,
        elevation: 5,
        backgroundColor: COLORS.Cyan,
        paddingVertical: 25
    },
    BoxText: {
        color: COLORS.White,
        marginTop: 5,
        fontSize: 13,
        width: 150
    },

    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    wrapper: {
        display: 'flex',
        marginRight: 60,
    },

    secondwrapper: {
        display: 'flex',
        flexDirection: 'row',
        width: 50,
        marginLeft: 20
    },

    containerwrapper: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})
export default TankList