import React, { Component } from 'react'
import {
    AppRegistry,
    StyleSheet,
    View,
    TouchableOpacity,
    Image
} from 'react-native'
import { Text, Card } from 'react-native-elements';
import { opacity } from 'react-native-reanimated/src/reanimated2/Colors';
import { COLORS } from '../components/theme';

const AboutScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Card containerStyle={styles.card}>
                <Text style={styles.textStyle}>
                    <Text style={{color:COLORS.Cyan, fontWeight:'bold'}}>H2OME WATER MONITORING</Text> shows the level of water for cubicle and cylindrical tanks. This system requires our nodemcu device to be placed top of the tank.
                </Text>
                <Text style={{color:COLORS.Cyan, marginTop: 20, marginLeft:10, fontWeight:'bold' }}>
                    FEATURES:
                </Text>
                    <Text style={{paddingLeft:20, marginTop: 5, ...styles.textStyle }}>
                        1. Show water level percentage.
                    </Text>
                    <Text style={{ paddingLeft:20,marginTop: 5, ...styles.textStyle }}>
                        2. Gives notification for below or above on every level of water.
                    </Text>
                    <Text style={{ paddingLeft:20,marginTop: 5, ...styles.textStyle }}>
                    3. Users can see the statistics for tank.(water volume, water height, empty space, level percentage etc)
                    </Text>
                    <Text style={{ paddingLeft:20,marginTop: 5, ...styles.textStyle }}>
                    4. User can add tank, edit tank parameters, delete tank, activate or deactivate tank.
                    </Text>
                    <Text style={{ paddingLeft:20,marginTop: 5, ...styles.textStyle }}>
                    5. User can reset password.
                    </Text>
                    <Text style={{ paddingLeft:20,marginTop: 5, ...styles.textStyle }}>
                    6. Admin can create multiple, edit existing user or delete user.
                    </Text>
                <Text style={{ color:COLORS.Cyan, marginTop: 20 , marginLeft:10, fontWeight:'bold'}}>
                    ADDITIONAL INFORMATION:
                </Text>
                <Text style={{paddingLeft:20,...styles.textStyle} }>
                    Updated: January 1, 2022
                </Text>
                <Text style={{paddingLeft:20,...styles.textStyle} }>
                    Size: 11M
                </Text>
                <Text style={{paddingLeft:20,...styles.textStyle} }>
                    Current Version: 1.1
                </Text>
                <Text style={ {paddingLeft:20,...styles.textStyle} }>
                    Requires Android: 8.0 and up
                </Text>
                <Text style={{paddingLeft:20,...styles.textStyle}}>
                    Developed By: Federal Urdu University CS Department
                </Text>
            </Card>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    card: {
        width: 380,
        height: 550,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: COLORS.Black,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 1.25,
        shadowRadius: 10.84,
        elevation: 5,
        padding: 5
    },

    btn: {
        marginTop: 30,
        width: 162,
        height: 50,
        backgroundColor: COLORS.Cyan,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
    },
    checkBoxText: {
        color: COLORS.White,
        marginLeft: 10,
        fontSize: 12,
    },
    iconMargin: {
        marginBottom: 5,
    },
    radioStyle: {
        marginLeft: 100,
        marginTop: 20,
    },
    textInputCustom: {
        fontSize: 12,
        borderColor: COLORS.White,
        borderBottomWidth: 1,
        width: 350,
        marginLeft: 5,
        marginTop: 15,
        color: COLORS.White,
    },

    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    wrapper: {
        display: 'flex',
        marginTop: 20,
        marginRight: 10,
    },

    secondwrapper: {
        display: 'flex',
        marginTop: 5,
        flexDirection: 'row',
    },

    containerwrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },

    textStyle: {
        fontSize: 12,
        color: COLORS.White,
        fontWeight: 'bold',
        marginLeft: 12,
    }
});

export default AboutScreen