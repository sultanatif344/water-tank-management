import React, { Component, useEffect, useState } from 'react'
import {
    AppRegistry,
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    ToastAndroid,
    ActivityIndicator,
} from 'react-native'
import { Text, Card, Icon, Rating, AirbnbRating } from 'react-native-elements';
import { COLORS } from '../components/theme';
import { getLocallyStoredUser, requestToResetPassword, sendReviewDetails } from '../helpers/user';
import { presentSuccessToast, presentFailureToast } from '../helpers/utility';
import Toast from 'react-native-toast-message';
const AddReview = () => {
    const [message, setUserMessage] = useState('');
    const [rating, setRating] = useState(3);
    const [localUser, setLocalUser] = useState({});
    const [loading, setLoading] = useState(false)

    const postReview = async (token, review) => {
        setLoading(true);
        const res = await sendReviewDetails(token, review);
        if (res.success == true) {
            presentSuccessToast(res.message);
            setLoading(false);
        }
        else {
            presentFailureToast(res.message);
            setLoading(false);
        }
    }

    const userSetter = async () => {
        const user = await getLocallyStoredUser();
        const locallyStoredUser = {};
        if (user != null) {
            console.log("this is the local", user)
            const storedUser = JSON.parse(user);
            setLocalUser(storedUser);
        }
    }

    const ratingCompleted = (rating) => {
        setRating(rating);
    }
    useEffect(() => {
        userSetter()
    }, [])

    return (
        <View>
            <Toast />
            <Card containerStyle={styles.card}>
                <Text style={styles.text}>Username</Text>
                <TextInput
                    style={styles.textInputCustom}
                    value={localUser.username}
                    placeholderTextColor={COLORS.White}
                    editable={false}
                />
                <Text style={{ marginTop: 20, marginBottom: 5, ...styles.text }}>Review Message</Text>
                <TextInput
                    style={{ backgroundColor: COLORS.White, color: 'black', textAlignVertical: 'top' }}
                    placeholderTextColor={COLORS.White}
                    onChangeText={(text) => setUserMessage(text)}
                    multiline={true}
                    numberOfLines={4}
                />
                < AirbnbRating
                    count={5}
                    reviews={["Terrible", "OK", "Good", "Very Good", "Amazing"]}
                    onFinishRating={ratingCompleted}
                    defaultRating={rating}
                    size={20}
                />
                <TouchableOpacity
                    style={message == ''? styles.disabledbtn : styles.btn}
                    disabled={message == '' ? true : false}
                    onPress={() => postReview(localUser.token, { username: localUser.username, description: message, rating: rating })}
                >
                    {loading == false ? <Text style={styles.btnText}>Submit</Text> : <ActivityIndicator animating={true} color={COLORS.DeepBlue}></ActivityIndicator>}
                </TouchableOpacity>
            </Card>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.DeepBlue,
    },

    welcomeContainer: {
        marginTop: 50,
        textAlign: "justify"
    },

    text: {
        color: COLORS.White,
        fontFamily: 'Trebuchet MS',
        fontWeight: 'bold'
    },

    subHeading: {
        marginLeft: 20,
        color: COLORS.White
    },

    textStyle: {
        color: COLORS.White,
    },

    btn: {
        marginTop: 30,
        width: 260,
        height: 50,
        backgroundColor: COLORS.Cyan,
        borderRadius: 100,
        paddingTop: 5,
        paddingBottom: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },

    disabledbtn: {
        marginTop: 30,
        width: 260,
        height: 50,
        backgroundColor: COLORS.White,
        borderRadius: 100,
        paddingTop: 5,
        paddingBottom: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },

    imgSize: {
        width: 200,
        height: 100,
        marginTop: 20,
        marginLeft: 100
    },
    btnText: {
        fontSize: 25,
        color: COLORS.DeepBlue,
        fontWeight: "100"
    },

    card: {
        height: 480,
        width: 350,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: COLORS.DeepBlue,
        paddingTop: 50,
        paddingBottom: 40,
        paddingLeft: 50,
        paddingRight: 70,
        marginBottom: 50,
        marginTop: 100,
        marginLeft: 30,
    },

    firstwrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        marginLeft: 40
    },

    textInputCustom: {
        fontSize: 15,
        borderColor: COLORS.White,
        borderBottomWidth: 2,
        width: 250,
        marginBottom: 10,
        color: COLORS.White,
        fontWeight: "100",
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },

    btnText: {
        fontSize: 15,
        color: "#000000",
        fontWeight: 'bold'
    },
})

export default AddReview