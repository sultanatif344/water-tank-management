import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput, Switch } from 'react-native';
import { COLORS } from '../components/theme';
import { Text, Card, Icon } from 'react-native-elements';
import { getDataOnAdd, updateMotorStatus } from '../helpers/firebase';

const MotorStatus = () => {
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => {
        updateMotorStatus(previousState => !previousState)
    }

    const getMotorStatusFromFirebase = (async () => {
        const motorStatus = await getDataOnAdd(new Date().getTime());

        setIsEnabled(motorStatus[1] == 1 ? true : false);
    })
    useEffect(() => {
        getMotorStatusFromFirebase();
    }, []);
    return (
        <View style={styles.container}>
            <Card containerStyle={styles.card}>
                <View style={styles.dividerStyle}>
                    {isEnabled ? <Text style={styles.textStyle}>Current Status: ON</Text> : <Text style={styles.textStyle}>Current Status: OFF</Text>}
                </View>

                <View style={styles.dividerStyle}>
                </View>
                {/* <View style={styles.switchContainer}>
                    <Switch
                        trackColor={{ false: "#03102B", true: "#03102B" }}
                        thumbColor={isEnabled ? "#53B9AB" : "#53B9AB"}
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={toggleSwitch}
                        value={isEnabled}
                        style={styles.switchStyle}
                    />
                </View>
                <View style={styles.dividerStyle}>
                </View> */}
            </Card>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    switchStyle: {
        width: 50,
        height: 40,
        marginBottom: 10,
        transform: [{ scaleX: 3 }, { scaleY: 3 }]
    },

    switchContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    dividerStyle: {
        borderTopColor: COLORS.White,
        borderTopWidth: 2,
        paddingTop: 33,
        paddingBottom: 33
    },

    textStyle: {
        fontSize: 25,
        textAlign: 'center'
    },
    card: {
        marginTop: 5,
        width: 300,
        height: 480,
        paddingTop: 110,
        backgroundColor: COLORS.Cyan,
        borderRadius: 10,
        shadowColor: COLORS.Black,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 1.25,
        shadowRadius: 10.84,
        elevation: 5
    },
})

export default MotorStatus