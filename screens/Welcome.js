import React, { Component } from 'react'
import {
    AppRegistry,
    StyleSheet,
    View,
    TouchableOpacity,
    Image
} from 'react-native'
import { Text } from 'react-native-elements';
import { opacity } from 'react-native-reanimated/src/reanimated2/Colors';
import { COLORS } from '../components/theme';

const WelcomeScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Image source={require('../assets/logo.png')} style={styles.imgSize} />
            <View style={styles.welcomeContainer}>
                <Text h1 style={styles.text}>Welcome!</Text>
                <Text h5 style={styles.subHeading}>Nice To See You Again.</Text>
                <TouchableOpacity
                    style={styles.btn}
                    onPress={() => navigation.navigate('SignIn')}
                >
                    <Text style={styles.btnText}>Login</Text>
                </TouchableOpacity>
            </View>
            <Image source={require('../assets/Images/curve1.png')} style={{position:'relative', opacity:0.1, top:-5, left:50}}/>
            <Image source={require('../assets/Images/curve2.png')} style={{position:'relative', left:5, bottom:270, width:420, height:400 }}/>
            <View style={{backgroundColor:'white', width:450, height:180, position:'absolute', top:650,zIndex:-1}}>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.DeepBlue,
    },

    welcomeContainer: {
        marginTop: 50,
        textAlign: "justify"
    },

    text: {
        color: COLORS.White,
        marginLeft: 20,
        fontFamily: 'Trebuchet MS',
        fontWeight: 'bold'
    },

    subHeading: {
        marginLeft: 20,
        color: COLORS.White
    },

    btn: {
        marginTop: 20,
        width: 162,
        height: 50,
        backgroundColor: COLORS.Cyan,
        borderRadius: 100,
        marginLeft: 25,
        paddingTop: 5,
        paddingBottom: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgSize: {
        width: 200,
        height: 100,
        marginTop: 20,
        marginLeft: 100
    },
    btnText: {
        fontSize: 25,
        color: COLORS.DeepBlue,
        fontWeight: "100"
    }

})

export default WelcomeScreen