import React, { Component, useEffect, useState } from 'react'
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native'
import { Text } from 'react-native-elements';
import DashboardHeader from '../components/Header'
import { COLORS } from '../components/theme';
import { getDataOnAdd, getDataOnce } from '../helpers/firebase';
import { getAllActiveTanksList, getTankFilledValueInPercentage } from '../helpers/tank';
import { getLocallyStoredUser } from '../helpers/user'
import { presentFailureToast } from '../helpers/utility';
import Toast from 'react-native-toast-message';
const DashboardScreen = ({ navigation }) => {
    const [waterFilled, setWaterFilled] = useState(0);
    const [activeTanks, setActiveTanks] = useState({});
    const [localUser, setLocalUser] = useState({});
    const getDataInPercentage = async () => {
        let activeTank = await getAllActiveTanksList();
        let tankFilledInfo = await getTankFilledValueInPercentage(activeTank.height);
        if (Object.keys(activeTank).length == 0) {
            presentFailureToast("There are no currently active tanks");
        }
        setWaterFilled(tankFilledInfo);
        setActiveTanks(activeTank);
    }

    const getLocalUser = async () => {
        const local = JSON.parse(await getLocallyStoredUser());
        setLocalUser(local);
      }
    // const setTankEmptyDistanceInLocal = async () => {
    //     const firebaseRes = await getDataFromFirebase();
    //     // let res = await setItemLocally(JSON.stringify({ waterfilled: firebaseRes.distance }), 'water_filled');
    //     setWaterFilled(firebaseRes.distance)
    // }
    useEffect(() => {
        getLocalUser();
        const unsubscribe = navigation.addListener('focus', () => {
            getDataInPercentage()
        });
        return unsubscribe;
    }, [navigation])

    return (
        <ScrollView style={styles.container}>
            <View style={{ position: 'relative', zIndex: 10 }}>
                <Toast />
            </View>
            <View>
                {waterFilled != undefined ? <DashboardHeader waterFilled={waterFilled} /> : <DashboardHeader waterFilled={0} />}
                <View style={styles.gridContainer}>
                    {localUser?.authorizations?.addATank == true ? (
                        <TouchableOpacity
                            style={styles.btn}
                            onPress={() => navigation.navigate('AddNewTank')}
                        >
                            <Image source={require('../assets/Images/tank.png')} width="40" height="40" />
                            <Text style={styles.btnText}>New Tank</Text>
                        </TouchableOpacity>
                    ) : (
                        () => {
                            return null;
                        }
                    )}

                    {localUser?.authorizations?.tankList == true ? (
                        <TouchableOpacity
                            style={styles.btn}
                            onPress={() => navigation.navigate('TankList')}
                        >
                            <Image source={require('../assets/Images/list.png')} width="40" height="40" />
                            <Text style={styles.btnText}>Tank List</Text>
                        </TouchableOpacity>
                    ) : (
                        () => {
                            return null;
                        }
                    )}

                    {localUser?.authorizations?.waterLevel == true ? (
                        <TouchableOpacity
                            style={styles.btn}
                            onPress={() => navigation.navigate('WaterLevels')}
                        >
                            <Image source={require('../assets/Images/water-level.png')} width="40" height="40" />
                            <Text style={styles.btnText}>Water Levels</Text>
                        </TouchableOpacity>
                    ) : (
                        () => {
                            return null;
                        }
                    )}

                    {localUser?.authorizations?.waterStatus == true ? (
                        <TouchableOpacity
                            style={styles.btn}
                            onPress={() => navigation.navigate('WaterStatus')}
                        >
                            <Image source={require('../assets/Images/drops.png')} width="40" height="40" />
                            <Text style={styles.btnText}>Water Status</Text>
                        </TouchableOpacity>
                    ) : (
                        () => {
                            return null;
                        }
                    )}
                    {localUser?.authorizations?.motorStatus == true ? (
                        <TouchableOpacity
                            style={styles.btn}
                            onPress={() => navigation.navigate('MotorStatus')}
                        >
                            <Image source={require('../assets/Images/ElectricMotor-electronics.png')} width="40" height="40" />
                            <Text style={styles.btnText}>Motor Status</Text>
                        </TouchableOpacity>
                    ) : (
                        () => {
                            return null;
                        }
                    )}

                    {localUser?.authorizations?.adminPanel == true ? (
                        <TouchableOpacity
                            style={styles.btn}
                            onPress={() => navigation.navigate('AdminPanel')}
                        >
                            <Image source={require('../assets/Images/admin.png')} width="40" height="40" />
                            <Text style={styles.btnText}>Admin Panel</Text>
                        </TouchableOpacity>
                    ) : (
                        () => {
                            return null;
                        }
                    )}

                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.Cyan,
    },

    gridContainer: {
        marginTop: 40,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingLeft: 50,
        paddingRight: 50,
    },
    imgSize: {
        width: 50,
        height: 50,
        marginBottom: 5
    },
    btn: {
        marginTop: 10,
        width: 130,
        height: 110,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 10,
        marginLeft: 2,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: COLORS.Blue,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 1.25,
        shadowRadius: 10.84,
        elevation: 5,
        paddingTop: 10
    },

    btnText: {
        fontSize: 11,
        margin: 8,
        color: COLORS.White,
        fontWeight: "100"
    }
})

export default DashboardScreen