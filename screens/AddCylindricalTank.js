import React, { useEffect, useState } from 'react';

import {
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Image,
} from 'react-native';
import { Input, Button, Card, Text } from 'react-native-elements';
import { COLORS } from '../components/theme';
import { useDispatch, useSelector } from 'react-redux';
import {
  CreateTankRequest,
  CreateTankSuccess,
  CreateTankFailure,
  UpdateTanksRequest,
  UpdateTanksSuccess,
  UpdateTanksFailure,
} from '../redux/tank/tankActions';
import { addTankDetails, getTanks, updateTankById } from '../helpers/tank';
import { presentFailureToast, presentSuccessToast } from '../helpers/utility';
import Toast from 'react-native-toast-message';
import { getLocallyStoredUser } from '../helpers/user';

const AddCylindricalTankPage = ({ navigation, route }) => {
  const userSelector = useSelector(state => state.user);
  const [tankHeight, setTankHeight] = useState(0);
  const [tankDiameter, setTankDiameter] = useState(0);
  const tankShape = 'Cylinderical';
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState({});
  const [tanksList, setTanksList] = useState([]);
  const [flag, setFlag] = useState(false);
  const [count, setCounter] = useState(0);
  // const tankDiameter = 0;
  const dispatch = useDispatch();

  const getLocalUserDetails = async () => {
    const localUser = JSON.parse(await getLocallyStoredUser());
    setUser(localUser);
  };

  const validateTank = async () => {
    const localUser = JSON.parse(await getLocallyStoredUser());
    const fetchedTanks = await getTanks(localUser.token);
    if (fetchedTanks.length == 2) {
      presentFailureToast("No of tanks limit is 2.")
      setFlag(true);
    }
  }
  useEffect(() => {
    getLocalUserDetails();
    if (route?.params?.isEdit == true) {
      const tank = route?.params?.item;
      setTankHeight(tank.height);
      setTankDiameter(tank.diameter);
    }
    else {
      const unsubscribe = navigation.addListener('focus', async () => {
        // The screen is focused
        // Call any action
        validateTank();
      });

      // Return the function to unsubscribe from the event so it gets removed on unmount
      return unsubscribe;
    }
  }, [navigation]);
  const sendTankDetails = tankDetails => {
    return async () => {
      setLoading(true);
      const fetchedTanks = await getTanks(user.token);
      if (fetchedTanks.length != 2 && flag == false) {
        console.log('these are the tank details', tankDetails);
        const tank = await addTankDetails(tankDetails, user.token);
        if (Object.keys(tank).length > 0) {
          setLoading(false);
          presentSuccessToast('Tank Added!');
        } else {
          setLoading(false);
          presentFailureToast('Operation Failed');
        }
      }
      else{
        setFlag(true);
        setLoading(false);
      }
    };
  };

  const updateCylindericalTank = (token, id, details) => {
    return async () => {
      setLoading(true);
      const res = await updateTankById(token, id, details);

      if (res.success == true) {
        setLoading(false);
        presentSuccessToast('Tank Updated!');
      } else {
        setLoading(false);
        presentFailureToast('Operation Failed');
      }
    };
  };
  return (
    <ScrollView>
      <Toast />
      <View style={styles.container}>
        <Image
          source={require('../assets/Images/cylinder.png')}
          width="50"
          height="50"
          style={{ marginTop: 50 }}
        />
        <Card containerStyle={styles.card}>
          <View style={styles.textContainerStyle}>
            <Text style={styles.textStyle}>Diameter:</Text>
            <TextInput
              value={String(tankDiameter)}
              style={styles.textInputCustom}
              keyboardType={"numeric"}
              onChangeText={text => setTankDiameter(Number(text))}
            />
          </View>
          <View style={styles.textContainerStyle}>
            <Text style={styles.textStyle}>Height:</Text>
            <TextInput
              value={String(tankHeight)}
              style={styles.textInputCustom}
              keyboardType={"numeric"}
              onChangeText={text => setTankHeight(Number(text))}
            />
          </View>
        </Card>
        {route.params?.isEdit == false ? (
          <View style={styles.center}>
            <TouchableOpacity
              style={(tankHeight == '' || tankDiameter == '') || flag == true ? styles.disabledbtn : styles.btn}
              disabled={(tankHeight == '' || tankDiameter == '') || flag == true ? true : false}
              onPress={sendTankDetails({
                height: tankHeight,
                diameter: tankDiameter,
                tankShape: tankShape,
              })}>
              {loading == false ? (
                <Text style={styles.btnText}>Add Tank</Text>
              ) : (
                <ActivityIndicator
                  animating={true}
                  color={COLORS.DeepBlue}></ActivityIndicator>
              )}
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.center}>
            <TouchableOpacity
              style={tankHeight == '' || tankDiameter == '' ? styles.disabledbtn : styles.btn}
              disabled={tankHeight == '' || tankDiameter == '' ? true : false}
              onPress={updateCylindericalTank(
                user.token,
                route?.params.item?._id,
                { height: tankHeight, diameter: tankDiameter },
              )}>
              {loading == false ? (
                <Text style={styles.btnText}>Update Tank</Text>
              ) : (
                <ActivityIndicator
                  animating={true}
                  color={COLORS.DeepBlue}></ActivityIndicator>
              )}
            </TouchableOpacity>
          </View>
        )}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  disabledbtn: {
    marginTop: 20,
    width: 162,
    height: 50,
    backgroundColor: COLORS.White,
    borderRadius: 100,
    paddingTop: 5,
    paddingBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  disabledbtnText: {
    fontSize: 25,
    color: COLORS.Black,
    fontWeight: '100',
  },
  card: {
    height: 250,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: COLORS.DeepBlue,
    paddingTop: 50,
    paddingBottom: 40,
    paddingLeft: 70,
    paddingRight: 70,
    marginBottom: 50,
  },

  textStyle: {
    color: COLORS.White,
  },

  textInputCustom: {
    fontSize: 15,
    borderColor: COLORS.White,
    borderBottomWidth: 2,
    width: 120,
    marginLeft: 10,
    marginBottom: 10,
    textAlign: 'center',
    color: COLORS.White,
    fontWeight: '100',
  },

  textContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  btn: {
    marginTop: 20,
    width: 162,
    height: 50,
    backgroundColor: COLORS.Cyan,
    borderRadius: 100,
    paddingTop: 5,
    paddingBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },

  btnText: {
    fontSize: 20,
    color: COLORS.BluishBlack,
    fontWeight: 'bold',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
});

// import React from 'react';

// import { View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
// import { Image, Input, Button, Card, Text } from 'react-native-elements';
// import { COLORS } from '../components/theme'
// const AddCylindricalTankPage = () => {
//     return (
//         <View style={styles.container}>
//             <Card containerStyle={styles.card}>
//                 <View style={styles.textContainerStyle}>
//                     <Text style={styles.textStyle}>Length:</Text>
//                     <TextInput
//                         style={styles.textInputCustom}
//                     />
//                 </View>
//                 <View style={styles.textContainerStyle}>
//                     <Text style={styles.textStyle}>Width:</Text>
//                     <TextInput
//                         style={styles.textInputCustom}
//                     />
//                 </View>
//                 <View style={styles.textContainerStyle}>
//                     <Text style={styles.textStyle}>Height:</Text>
//                     <TextInput
//                         style={styles.textInputCustom}
//                     />
//                 </View>
//             </Card>
//             <View style={styles.center}>
//                 <TouchableOpacity
//                     style={styles.btn}
//                 >
//                     <Text style={styles.btnText}>Add Tank</Text>
//                 </TouchableOpacity>
//             </View>
//         </View>
//     )
// }

// const styles = StyleSheet.create({
//     container: {
//         justifyContent: 'center',
//         alignItems: 'center',
//     },
//     card: {
//         flex: 1,
//         borderBottomLeftRadius: 20,
//         borderBottomRightRadius: 20,
//         borderTopLeftRadius: 20,
//         borderTopRightRadius: 20,
//         backgroundColor: COLORS.DeepBlue,
//         paddingTop: 50,
//         paddingBottom: 50,
//         paddingLeft: 30,
//         paddingRight: 30
//     },

//     textStyle: {
//         color: COLORS.White,
//     },

//     textInputCustom: {
//         fontSize: 15,
//         borderColor: COLORS.White,
//         borderBottomWidth: 2,
//         width: 120,
//         marginLeft: 10,
//         marginBottom: 10,
//         color: COLORS.White,
//         fontWeight: "100"
//     },

//     textContainerStyle: {
//         flex: 1,
//         flexDirection: "row",
//         justifyContent: "space-between",
//         alignItems: "center"
//     },

//     btn: {
//         marginTop: 20,
//         width: 162,
//         height: 50,
//         backgroundColor: COLORS.Cyan,
//         borderRadius: 100,
//         paddingTop: 5,
//         paddingBottom: 5,
//         justifyContent: 'center',
//         alignItems: 'center'
//     },

//     btnText: {
//         fontSize: 20,
//         color: COLORS.BluishBlack,
//         fontWeight: 'bold'
//     },
//     center: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: "flex-start"
//     }
// })

export default AddCylindricalTankPage;
