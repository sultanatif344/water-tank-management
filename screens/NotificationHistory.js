import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { getAllUsers, getAllNotifications, getLocallyStoredUser } from '../helpers/user'
import { useSelector, useDispatch } from 'react-redux';
// import { GetTankRequest, GetTankSuccess, GetTankFailure } from '../redux/tank/tankActions';
import { GetUsersRequest, GetUsersSuccess, GetUsersFailure } from '../redux/user/userActions';

// import TankListTvc from '../components/tanklisttvc';
import UserListTvc from '../components/userlisttvc';
import NotificationHistoryListTvc from '../components/notificationHistoryListtvc';

const NotificationHistory = ({ }) => {

    const [notificationList, setNotifications] = useState([]);
    const [user, setLocallyStoredUser] = useState({});

    // useEffect(() => {

    // },[navigation])

    // const userSelector = useSelector((state) => state.user);
    const LocallyStoreUser = async () => {
        const localUser = JSON.parse(await getLocallyStoredUser());
        getAllNotificationslist(localUser.token)
    }
    const getAllNotificationslist = async (token) => {
        const notifications = await getAllNotifications(token);
        console.log("these are my notifications", notifications);
        setNotifications(notifications)
    }

    useEffect(() => {
        LocallyStoreUser();
    }, []);
    return (
        <FlatList
            data={notificationList}
            renderItem={({ item }) =>
                (<NotificationHistoryListTvc notificationParam={item} />)
            }
            keyExtractor={(item, index) => index.toString()}
        />
    )
}

export default NotificationHistory;