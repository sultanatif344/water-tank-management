import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Card, Text } from 'react-native-elements';
import { COLORS } from '../components/theme';
import { waterLevelObj } from '../components/global'
import { getTanks } from '../helpers/tank';
import { getLocallyStoredItem } from '../helpers/utility';
import { getLocallyStoredUser } from '../helpers/user'
import { getTankFilledValueInPercentage, getAllActiveTanksList } from '../helpers/tank'
import { getDataOnAdd, getDataOnce } from '../helpers/firebase';
const WaterLevels = () => {
    const [totalHeight, setTotalHeight] = useState(0);
    const [emptyDistance, setEmptyDistance] = useState(0);
    const [waterHeight, setWaterHeight] = useState(0);
    const [totalWaterHeightInPercent, settotalWaterHeightInPercent] = useState(0);
    const [activeTank, setActiveTank] = useState({});
    const [dummy, setDummy] = useState();

    useEffect(() => {
        if (Object.keys(activeTank).length == 0) {
            // getAllActiveTanksList();
            getAllActiveTanksList().then(res => {
                setActiveTank(res);
            })
            setActiveTank()
            console.log("water distance", waterLevelObj.distance);
        }
    }, [])

    // getAllActiveTanksList = async () => {
    //     const user = JSON.parse(await getLocallyStoredUser());
    //     if (user != null) {
    //         var tanksList = await getTanks(user.token);
    //         tanksList = tanksList.filter((element) => element.activated == true);
    //         if (tanksList.length > 0) {
    //             console.log("Tankslist", tanksList);
    //             // setActiveTank(tanksList[0]);
    //         }
    //     }
    // }

    // const showTankFilledPercentageValue = async (height) => {

    // }
    // const getTankFilledValueInPercentage = async (height,distance) => {
    //     let date = new Date().getTime();
    //     const firebaseRes = await getDataOnce(date);
    //     let distancePercentage = (activeTank.height - firebaseRes.distance) / activeTank.height * 100 | 0;
    //     waterLevelObj.distance = distancePercentage;
    //     console.log('distancePercentage', distancePercentage);
    //     settotalWaterHeightInPercent(distancePercentage);

    // }
    useEffect(() => {
        if (activeTank) {
            // showTankFilledPercentageValue(activeTank.height)
            getTankFilledValueInPercentage(activeTank.height).then(res => {
            console.log("this is the tanks filled info",res)
                settotalWaterHeightInPercent(res);
            })
        }
    }, [activeTank])
    return (
        <View style={styles.container}>
            <Card containerStyle={[styles.hundredPercentCard, { backgroundColor: totalWaterHeightInPercent >= 100 ? COLORS.Cyan : COLORS.White }, {width:350}]}>
                <Text style={styles.textStyle}>100%</Text>
            </Card>
            <Card containerStyle={[styles.seventyFivePercentCard, { backgroundColor: totalWaterHeightInPercent >= 75 ? COLORS.Cyan : COLORS.White }, {width:350}]}>
                <Text style={styles.textStyle}>75%</Text>
            </Card>
            <Card containerStyle={[styles.fiftyPercentCard, { backgroundColor: totalWaterHeightInPercent >= 50 ? COLORS.Cyan : COLORS.White }, {width:350}]}>
                <Text style={styles.textStyle}>50%</Text>
            </Card>
            <Card containerStyle={[styles.twentyFivePercentCard, { backgroundColor: totalWaterHeightInPercent >= 25 ? COLORS.Cyan : COLORS.White }, {width:350}]}>
                <Text style={styles.textStyle}>25%</Text>
            </Card>
        </View>
    )



}




const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center', 
        alignItems:'center',
    },
    twentyFivePercentCard: {
        textAlign: 'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: COLORS.Black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 2,
        shadowRadius: 10.84,
        elevation: 5,

    },

    fiftyPercentCard: {
        textAlign: 'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: COLORS.Black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 2,
        shadowRadius: 10.84,
        elevation: 5,
        backgroundColor: waterLevelObj.distance >= 50 ? COLORS.Cyan : COLORS.White
    },

    seventyFivePercentCard: {
        textAlign: 'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: COLORS.Black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 2,
        shadowRadius: 10.84,
        elevation: 5,
        backgroundColor: waterLevelObj.distance >= 75 ? COLORS.Cyan : COLORS.White
    },

    hundredPercentCard: {
        textAlign: 'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: COLORS.Black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 2,
        shadowRadius: 10.84,
        elevation: 5,
        backgroundColor: waterLevelObj.distance == 100 ? COLORS.Cyan : COLORS.White
    },
    textStyle: {
        fontSize: 35,
        fontWeight: 'bold',
        textAlign:'center'
    }
})
export default WaterLevels