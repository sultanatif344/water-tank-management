import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput, ActivityIndicator } from 'react-native';
import { COLORS } from '../components/theme';
import { Text, Card, Icon } from 'react-native-elements';
import CheckBox from 'react-native-check-box';
import { signUp, updateUserById } from '../helpers/user';
import { connect, useDispatch, useSelector } from 'react-redux';
import {
  SignUpUserRequest,
  SignUpUserSuccess,
  SignUpUserFailure,
  UpdateUserRequest,
  UpdateUserSuccess,
  UpdateUserFailure,
} from '../redux/user/userActions';
import RadioForm from 'react-native-simple-radio-button';
import { getLocallyStoredUser } from '../helpers/user'
import { presentFailureToast, presentSuccessToast } from '../helpers/utility';
import Toast from 'react-native-toast-message'
import { Formik } from 'formik';
import * as yup from 'yup';
const NewUser = ({ route }) => {
  const [authorizations, setAuthorizations] = useState({
    waterLevel: false,
    waterStatus: false,
    addATank: false,
    tankList: false,
    motorStatus: false,
    adminPanel: false,
  });
  const userSelector = useSelector(state => state.user);

  const [userName, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [localUser, setLocalUser] = useState({});
  const [valid, setValid] = useState(false);
  const [userNameIsFilled, setUserNameIsFilled] = useState(null);
  const [emailIsFilled, setEmailIsFilled] = useState(null);
  const [passwordIsFilled, setPasswordIsFilled] = useState(null);
  const [confirmPasswordIsFilled, setConfirmPasswordIsFilled] = useState(null);
  const [loading, setLoading] = useState(false);
  const [togglePasswordShow, setTogglePasswordShow] = useState(true);
  const [toggleConfirmPasswordShow, setToggleConfirmPasswordShow] = useState(true);
  const [confirmPasswordError, setConfirmPasswordError] = useState(false);
  const signUpValidationSchema = yup.object().shape({
    username: yup
      .string()
      .required("Username Is Required"),
    email: yup
      .string()
      .email("Please Enter Valid Email")
      .required("Email Address Is Required"),
    password: yup
      .string()
      .min(8, ({ min }) => `Password must be at least ${min} characters`)
      .required('Password is required')
  })
  const updationValidationSchema = yup.object().shape({
    username: yup
      .string()
      .required("Username Is Required"),
    email: yup
      .string()
      .email("Please Enter Valid Email")
      .required("Email Address Is Required")
  })
  let user;
  let isEdit;
  const userType = [
    { label: 'Admin   ', value: 'Admin' },
    { label: 'User', value: 'User' },
  ];
  const [selectedUserType, setSelectedUserType] = useState('Admin');
  const details = {
    userType: selectedUserType,
    authorizations: authorizations,
  };
  const dispatch = useDispatch();
  useEffect(() => {
    user = route.params?.item;
    isEdit = route.params?.isEdit;
    if (isEdit == true) {
      console.log('parameters', { user });
      console.log('isEdit', isEdit);
      setUserName(user.username);
      setEmail(user.email);
      setSelectedUserType(user.userType);
      setAuthorizations({
        waterLevel: user.authorizations.waterLevel,
        waterStatus: user.authorizations.waterStatus,
        addATank: user.authorizations.addATank,
        tankList: user.authorizations.tankList,
        motorStatus: user.authorizations.motorStatus,
        adminPanel: user.authorizations.adminPanel,
      });
      setValid(true);
    }
    else{
      presentFailureToast("Please Complete All Fields");
    }
    getLocalUser();
  }, []);

  useEffect(() => {
    // if (authorizations.addATank == true || authorizations.adminPanel == true || authorizations.motorStatus == true || authorizations.tankList == true || authorizations.waterLevel == true || authorizations.waterStatus == true) {
    //   validate();
    // }
  }, [])

  const validate = async (propValues) => {
    if (route.params?.isEdit == false) {
      if (propValues.password != '' && confirmPassword != '') {
        if (propValues.password == confirmPassword) {
          setValid(true);
        }
        else {
          setConfirmPasswordError(true)
          setValid(false);
        }
      }
      else {
        setValid(false);
      }
    }
  }
  const getLocalUser = async () => {
    const local = JSON.parse(await getLocallyStoredUser());
    setLocalUser(local);
  }
  const signUpUser = propValues => {
    return async () => {
      setLoading(true);
      propValues["authorizations"] = details["authorizations"];
      propValues["userType"] = details["userType"];

      // if (propValues.password == confirmPassword) {} else {
      //   presentFailureToast('Passwords do not match');
      //   setLoading(false);
      // }
        dispatch(SignUpUserRequest);
        const res = await signUp(propValues);
        if (res.success == true) {
          dispatch(SignUpUserSuccess);
          presentSuccessToast('Sign Up Succeeded');
          setLoading(false);
        } else {
          dispatch(SignUpUserFailure('Sign Up failed'));
          setLoading(false);
        }
      
    };
  };

  const updateUser = (token, id, propValues) => {
    return async () => {
      dispatch(UpdateUserRequest);
      setLoading(true);
      propValues["authorizations"] = details["authorizations"];
      propValues["userType"] = details["userType"];
      const res = await updateUserById(token, id, propValues);

      if (res.success == true) {
        dispatch(UpdateUserSuccess);
        presentSuccessToast('User Information Updated!');
        setLoading(false);
      } else {
        dispatch(UpdateUserFailure('Updation Failed'));
        presentFailureToast('Updation Failed!');
        setLoading(false);
      }
    };
  };
  return (
    <View style={styles.container}>
      <View style={{ position: 'relative', zIndex: 10 }}>
        <Toast />
      </View>
      <View>
        <Card containerStyle={styles.card}>
          <Icon
            name="user-circle-o"
            type="font-awesome"
            color="#ffffff"
            size={70}
            style={{ marginTop: 50 }}
          />
          <Formik
            validationSchema={route.params?.isEdit == false ? signUpValidationSchema : updationValidationSchema}
            initialValues={route.params?.isEdit == false ? { username: '', email: '', password: '' } : { username: route.params?.item?.username, email: route.params?.item?.email }}

          >
            {(props,
              isValid) => {
              return (
                <View style={styles.iconMargin}>
                  <TextInput
                    placeholder="User Name*"
                    placeholderTextColor={COLORS.White}
                    value={props.values.username}
                    onBlur={props.handleBlur('email')}
                    style={styles.textInputCustom}
                    onTouchStart={() => props.values.username != '' ? setUserNameIsFilled(true) : setUserNameIsFilled(false)}
                    onChangeText={props.handleChange('username')}
                  />
                  {props.errors?.username &&
                    <Text style={{ fontSize: 10, color: 'red' }}>{props.errors?.username}</Text>
                  }
                  <TextInput
                    placeholder="Email*"
                    placeholderTextColor={COLORS.White}
                    value={props.values.email}
                    onBlur={props.handleBlur('email')}
                    style={styles.textInputCustom}
                    onTouchStart={() => props.values.email != '' ? setEmailIsFilled(true) : setEmailIsFilled(false)}
                    onChangeText={props.handleChange('email')}
                  />
                  {props.errors?.email &&
                    <Text style={{ fontSize: 10, color: 'red' }}>{props.errors?.email}</Text>
                  }
                  {route.params?.isEdit == false ? (
                    <View>
                      <TextInput
                        placeholder="Password*"
                        placeholderTextColor={COLORS.White}
                        style={styles.textInputCustom}
                        onBlur={props.handleBlur('password')}
                        onTouchStart={() => props.values.password != '' ? setUserNameIsFilled(true) : setUserNameIsFilled(false)}
                        onChangeText={props.handleChange('password')}
                        secureTextEntry={togglePasswordShow}
                      />
                      {props.errors.password &&
                        <Text style={{ fontSize: 10, color: 'red' }}>{props.errors.password}</Text>
                      }
                      {togglePasswordShow == false ? (
                        <TouchableOpacity onPress={() => setTogglePasswordShow(true)} style={{
                          position: 'absolute',
                          right: 10,
                          top: 27,
                          bottom: 30,
                          height: 30,
                          backgroundColor: 'transparent'
                        }}>
                          <Icon type="entypo" name="eye" size={20} color={COLORS.White} />
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity onPress={() => setTogglePasswordShow(false)} style={{
                          position: 'absolute',
                          right: 10,
                          top: 27,
                          bottom: 30,
                          height: 30,
                          backgroundColor: 'transparent'
                        }}>
                          <Icon type="entypo" name="eye-with-line" size={20} color={COLORS.White} />
                        </TouchableOpacity>
                      )}
                    </View>
                  ) : (
                    () => {
                      return null;
                    }
                  )}
                  

                  <View style={styles.containerwrapper}>
                    <View style={styles.wrapper}>
                      <View style={styles.secondwrapper}>
                        <CheckBox
                          onClick={() => {
                            setAuthorizations({
                              waterLevel: authorizations.waterLevel,
                              waterStatus: authorizations.waterStatus,
                              addATank: !authorizations.addATank,
                              tankList: authorizations.tankList,
                              motorStatus: authorizations.motorStatus,
                              adminPanel: authorizations.adminPanel,
                            });
                            const validValue = false;
                            setValid(!validValue);
                          }}
                          isChecked={authorizations.addATank}
                          uncheckedCheckBoxColor={COLORS.White}
                          checkedCheckBoxColor={COLORS.Cyan}
                        />
                        <Text style={styles.checkBoxText}>Add Tank</Text>
                      </View>

                      <View style={styles.secondwrapper}>

                        <CheckBox
                          onClick={() => {
                            setAuthorizations({
                              waterLevel: !authorizations.waterLevel,
                              waterStatus: authorizations.waterStatus,
                              addATank: authorizations.addATank,
                              tankList: authorizations.tankList,
                              motorStatus: authorizations.motorStatus,
                              adminPanel: authorizations.adminPanel,
                            });
                            const validValue = false;
                            setValid(!validValue);
                          }}
                          isChecked={authorizations.waterLevel}
                          uncheckedCheckBoxColor={COLORS.White}
                          checkedCheckBoxColor={COLORS.Cyan}
                        />
                        <Text style={styles.checkBoxText}>Water Level</Text>
                      </View>

                      <View style={styles.secondwrapper}>
                        <CheckBox
                          onClick={() => {
                            setAuthorizations({
                              waterLevel: authorizations.waterLevel,
                              waterStatus: authorizations.waterStatus,
                              addATank: authorizations.addATank,
                              tankList: authorizations.tankList,
                              motorStatus: !authorizations.motorStatus,
                              adminPanel: authorizations.adminPanel,
                            });
                            const validValue = false;
                            setValid(!validValue);
                          }}
                          isChecked={authorizations.motorStatus}
                          uncheckedCheckBoxColor={COLORS.White}
                          checkedCheckBoxColor={COLORS.Cyan}
                        />
                        <Text style={styles.checkBoxText}>Motor Status</Text>
                      </View>
                    </View>
                    <View style={styles.wrapper}>
                      <View style={styles.secondwrapper}>
                        <CheckBox
                          onClick={() => {
                            setAuthorizations({
                              waterLevel: authorizations.waterLevel,
                              waterStatus: authorizations.waterStatus,
                              addATank: authorizations.addATank,
                              tankList: !authorizations.tankList,
                              motorStatus: authorizations.motorStatus,
                              adminPanel: authorizations.adminPanel,
                            });
                            const validValue = false;
                            setValid(!validValue);
                          }}
                          isChecked={authorizations.tankList}
                          uncheckedCheckBoxColor={COLORS.White}
                          checkedCheckBoxColor={COLORS.Cyan}
                        />
                        <Text style={styles.checkBoxText}>Tank List</Text>
                      </View>

                      <View style={styles.secondwrapper}>
                        <CheckBox
                          onClick={() => {
                            setAuthorizations({
                              waterLevel: authorizations.waterLevel,
                              waterStatus: !authorizations.waterStatus,
                              addATank: authorizations.addATank,
                              tankList: authorizations.tankList,
                              motorStatus: authorizations.motorStatus,
                              adminPanel: authorizations.adminPanel,
                            });
                            const validValue = false;
                            setValid(!validValue);
                          }}
                          isChecked={authorizations.waterStatus}
                          uncheckedCheckBoxColor={COLORS.White}
                          checkedCheckBoxColor={COLORS.Cyan}
                        />
                        <Text style={styles.checkBoxText}>Water Status</Text>
                      </View>

                      <View style={styles.secondwrapper}>
                        <CheckBox
                          onClick={() => {
                            setAuthorizations({
                              waterLevel: authorizations.waterLevel,
                              waterStatus: authorizations.waterStatus,
                              addATank: authorizations.addATank,
                              tankList: authorizations.tankList,
                              motorStatus: authorizations.motorStatus,
                              adminPanel: !authorizations.adminPanel,
                            });
                            const validValue = false;
                            setValid(!validValue);
                          }}
                          isChecked={authorizations.adminPanel}
                          uncheckedCheckBoxColor={COLORS.White}
                          checkedCheckBoxColor={COLORS.Cyan}
                        />
                        <Text style={styles.checkBoxText}>Admin Panel</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.radioStyle}>
                    <RadioForm
                      buttonColor={COLORS.Cyan}
                      selectedLabelColor={COLORS.White}
                      labelColor={'white'}
                      formHorizontal={true}
                      radio_props={userType}
                      selectedButtonColor={COLORS.Cyan}
                      initial={
                        route.params?.isEdit == true
                          ? userType.findIndex(
                            userType =>
                              userType.value == route.params?.item?.userType,
                          )
                          : 0
                      }
                      onPress={value => setSelectedUserType(value)}
                    />
                  </View>
                  {route.params?.isEdit == true ? (
                    <View style={styles.center}>
                      <TouchableOpacity
                        style={!props.isValid || valid == false ? styles.disabledbtn : styles.btn}
                        disabled={!props.isValid || valid == false ? true : false}
                        onPress={updateUser(
                          localUser.token,
                          route.params?.item?._id,
                          props.values,
                        )}>
                        {loading == false ? (
                          <Text style={styles.btnText}>Update User</Text>
                        ) : (
                          <ActivityIndicator
                            animating={true}
                            color={COLORS.BluishBlack}></ActivityIndicator>
                        )}
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <View style={styles.center}>
                      <TouchableOpacity
                        style={!props.isValid || valid == false ? styles.disabledbtn : styles.btn}
                        disabled={!props.isValid || valid == false ? true : false}
                        onPress={signUpUser(props.values)}
                      >
                        {loading == false ? (
                          <Text style={styles.btnText}>Add User</Text>
                        ) : (
                          <ActivityIndicator
                            animating={true}
                            color={COLORS.BluishBlack}></ActivityIndicator>
                        )}
                      </TouchableOpacity>
                    </View>
                  )}
                </View>
              )
            }}


          </Formik>

        </Card>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  card: {
    marginTop: 5,
    width: 380,
    height: 600,
    backgroundColor: COLORS.DeepBlue,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: COLORS.Black,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 1.25,
    shadowRadius: 10.84,
    elevation: 5,
  },

  btn: {
    marginBottom: 150,
    width: 162,
    height: 50,
    backgroundColor: COLORS.Cyan,
    borderRadius: 50,
    color: COLORS.White,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    fontSize: 15,
    fontWeight: 'bold',
    color: COLORS.White
  },
  checkBoxText: {
    color: COLORS.White,
    marginLeft: 10,
    fontSize: 12,
  },
  iconMargin: {
    marginBottom: 5,
  },
  radioStyle: {
    marginLeft: 100,
    marginTop: 20,
  },
  textInputCustom: {
    fontSize: 12,
    borderColor: COLORS.White,
    borderBottomWidth: 1,
    width: 350,
    marginLeft: 5,
    marginTop: 15,
    color: COLORS.White,
  },

  validInputCustom: {
    fontSize: 12,
    borderColor: COLORS.Cyan,
    borderBottomWidth: 1,
    width: 350,
    marginLeft: 5,
    marginTop: 15,
    color: COLORS.White,
  },

  invalidInputCustom: {
    fontSize: 12,
    borderColor: COLORS.Red,
    borderBottomWidth: 1,
    width: 350,
    marginLeft: 5,
    marginTop: 15,
    color: COLORS.White,
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  wrapper: {
    display: 'flex',
    marginTop: 20,
    marginRight: 10,
  },

  secondwrapper: {
    display: 'flex',
    marginTop: 5,
    flexDirection: 'row',
  },

  containerwrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  disabledbtn: {
    width: 162,
    height: 50,
    borderRadius: 50,
    marginBottom: 150,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.White,
    color: COLORS.Black,
    fontSize: 15,
    fontWeight: 'bold',
    paddingTop: 5,
    paddingBottom: 5,
  },
});
export default NewUser;
