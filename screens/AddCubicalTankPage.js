import React, { useState, useEffect } from 'react';

import {
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator,
} from 'react-native';
import { Input, Button, Card, Text } from 'react-native-elements';
import { COLORS } from '../components/theme';
import { useDispatch, useSelector } from 'react-redux';
import {
  CreateTankRequest,
  CreateTankSuccess,
  CreateTankFailure,
  UpdateTanksRequest,
  UpdateTanksSuccess,
  UpdateTanksFailure,
} from '../redux/tank/tankActions';
import { addTankDetails, getTanks, updateTankById } from '../helpers/tank';
import Toast from 'react-native-toast-message';
import { presentFailureToast, presentSuccessToast } from '../helpers/utility';
import { getLocallyStoredUser } from '../helpers/user';
const AddCubicalTankPage = ({ navigation, route }) => {
  const userSelector = useSelector(state => state.user);
  const [tankHeight, setTankHeight] = useState(0);
  const [tankWidth, setTankWidth] = useState(0);
  const [tankLength, setTankLength] = useState(0);
  const [loading, setLoading] = useState(false);
  const tankShape = 'Cubical';
  const tankDiameter = 0;
  const [user, setUser] = useState({});
  const [tanksList, setTanksList] = useState([]);
  const [flag, setFlag] = useState(false);
  const dispatch = useDispatch();

  const getLocalUserDetails = async () => {
    const localUser = JSON.parse(await getLocallyStoredUser());
    setUser(localUser);
  };

  const validateTank = async () => {
    const localUser = JSON.parse(await getLocallyStoredUser());
    const fetchedTanks = await getTanks(localUser.token);
    if (fetchedTanks.length == 2) {
      presentFailureToast("No of tanks limit is 2.")
      setFlag(true);
    }
  }
  useEffect(() => {
    getLocalUserDetails();
    if (route.params?.isEdit == true) {
      const tank = route?.params?.item;
      console.log('this is the tank', tank);
      setTankHeight(tank.height);
      setTankWidth(tank.width);
      setTankLength(tank.length);
    }
    else {
      const unsubscribe = navigation.addListener('focus', async () => {
        // The screen is focused
        // Call any action
        validateTank();
      });

      // Return the function to unsubscribe from the event so it gets removed on unmount
      return unsubscribe;
    }
  }, [navigation]);
  const sendTankDetails = tankDetails => {
    return async () => {
      setLoading(true);
      const fetchedTanks = await getTanks(user.token);
      if (flag == false) {
        console.log('these are the tank details', tankDetails);
        const tank = await addTankDetails(tankDetails, user.token);
        if (Object.keys(tank).length > 0) {
          setLoading(false);
          presentSuccessToast('Tank Added!');
        } else {
          setLoading(false);
          presentFailureToast('Operation Failed');
        }
      }
      else{
        setFlag(true);
        setLoading(false);
      }
    };
  };


  const updateCubicalTank = (token, id, details) => {
    return async () => {
      setLoading(true);
      const res = await updateTankById(token, id, details);

      if (res.success == true) {
        setLoading(false);
        presentSuccessToast('Tank Updated!');
      } else {
        setLoading(false);
        presentFailureToast('Operation Failed');
      }
    };
  };
  return (
    <ScrollView>
      <Toast />
      <View style={styles.container}>
        <Image
          source={require('../assets/Images/cubicle.png')}
          width="50"
          height="50"
          style={{ marginTop: 50 }}
        />
        <Card containerStyle={styles.card}>
          <View style={styles.textContainerStyle}>
            <Text style={styles.textStyle}>Length:</Text>
            <TextInput
              value={String(tankLength)}
              style={styles.textInputCustom}
              keyboardType={"numeric"}
              onChangeText={text => setTankLength(Number(text))}
            />
          </View>
          <View style={styles.textContainerStyle}>
            <Text style={styles.textStyle}>Width:</Text>
            <TextInput
              value={String(tankWidth)}
              style={styles.textInputCustom}
              keyboardType={"numeric"}
              onChangeText={text => setTankWidth(Number(text))}
            />
          </View>
          <View style={styles.textContainerStyle}>
            <Text style={styles.textStyle}>Height:</Text>
            <TextInput
              value={String(tankHeight)}
              style={styles.textInputCustom}
              keyboardType={"numeric"}
              onChangeText={text => setTankHeight(Number(text))}
            />
          </View>
        </Card>
        {route.params?.isEdit == false ? (
          <View style={styles.center}>
            <TouchableOpacity
              style={(tankWidth == '' || tankHeight == '' || tankLength == '') || flag == true ? styles.disabledbtn : styles.btn}
              disabled={(tankWidth == '' || tankHeight == '' || tankLength == '') || flag == true ? true : false}
              onPress={sendTankDetails({
                width: tankWidth,
                height: tankHeight,
                length: tankLength,
                tankShape: tankShape,
                diameter: tankDiameter,
              })}>
              {loading == false ? (
                <Text style={styles.btnText}>Add Tank</Text>
              ) : (
                <ActivityIndicator
                  animating={true}
                  color={COLORS.DeepBlue}></ActivityIndicator>
              )}
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.center}>
            <TouchableOpacity
              style={tankWidth == '' || tankHeight == '' || tankLength == '' ? styles.disabledbtn : styles.btn}
              disabled={tankWidth == '' || tankHeight == '' || tankLength == '' ? true : false}
              onPress={updateCubicalTank(user.token, route?.params.item?._id, {
                width: tankWidth,
                height: tankHeight,
                length: tankLength,
                diameter: tankDiameter,
              })}>
              {loading == false ? (
                <Text style={styles.btnText}>Update Tank</Text>
              ) : (
                <ActivityIndicator
                  animating={true}
                  color={COLORS.DeepBlue}></ActivityIndicator>
              )}
            </TouchableOpacity>
          </View>
        )}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    height: 250,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: COLORS.DeepBlue,
    paddingTop: 50,
    paddingBottom: 40,
    paddingLeft: 70,
    paddingRight: 70,
    marginBottom: 50,
  },

  textStyle: {
    color: COLORS.White,
  },

  textInputCustom: {
    fontSize: 15,
    borderColor: COLORS.White,
    borderBottomWidth: 2,
    width: 120,
    marginLeft: 10,
    marginBottom: 10,
    textAlign: 'center',
    color: COLORS.White,
    fontWeight: '100',
  },

  textContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  btn: {
    marginTop: 20,
    width: 162,
    height: 50,
    backgroundColor: COLORS.Cyan,
    borderRadius: 100,
    paddingTop: 5,
    paddingBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  disabledbtn: {
    marginTop: 20,
    width: 162,
    height: 50,
    backgroundColor: COLORS.White,
    borderRadius: 100,
    paddingTop: 5,
    paddingBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  disabledbtnText: {
    fontSize: 25,
    color: COLORS.Black,
    fontWeight: '100',
  },
  btnText: {
    fontSize: 20,
    color: COLORS.BluishBlack,
    fontWeight: 'bold',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
});

export default AddCubicalTankPage;
