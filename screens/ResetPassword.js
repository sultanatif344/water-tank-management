import React, { Component, useEffect, useState } from 'react'
import {
    AppRegistry,
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    ToastAndroid,
    ActivityIndicator,
    ScrollView
} from 'react-native'
import { Text, Card, Icon } from 'react-native-elements';
import { COLORS } from '../components/theme';
import { getLocallyStoredUser, requestToResetPassword } from '../helpers/user';
import { presentSuccessToast, presentFailureToast } from '../helpers/utility';
import Toast from 'react-native-toast-message';
const ResetPasswordScreen = () => {
    const [oldPassword, setOldUserPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [localUser, setLocalUser] = useState({});
    const [loading, setLoading] = useState(false)
    const [toggleOldPasswordShow, setToggleOldPasswordShow] = useState(true);
    const [toggleNewPasswordShow, setToggleNewPasswordShow] = useState(true);
    const [toggleConfirmPasswordShow, setToggleConfirmPasswordShow] = useState(true);

    const changePassword = async (token, id, oldPassword, newPassword) => {
        setLoading(true);
        requestToResetPassword(token, id, oldPassword, newPassword)
            .then(res => {
                console.log("my res", res)
                if (res.updated == true) {
                    setLoading(false)
                    presentSuccessToast('User Password Updated');
                }
                else {
                    setLoading(false)
                    presentFailureToast('Password Updation Failed');
                }
            })
    }

    const userSetter = async () => {
        const user = await getLocallyStoredUser();
        const locallyStoredUser = {};
        if (user != null) {
            console.log("this is the local", user)
            const storedUser = JSON.parse(user);
            setLocalUser(storedUser);
        }
    }
    useEffect(() => {
        userSetter()
    }, [])

    return (
        <ScrollView>
            <Toast />
            <Card containerStyle={styles.card}>
                <View style={styles.firstwrapper}>
                    <Icon name='lock' type='font-awesome' size={100} color={COLORS.White} />
                    <Icon name='refresh' type='font-awesome' size={40} color={COLORS.White} style={{ marginTop: 50, marginLeft: 10 }} />
                </View>
                <View style={{ justifyContent: 'center', }}>
                    <TextInput
                        style={styles.textInputCustom}
                        placeholder='Old Password'
                        placeholderTextColor={COLORS.White}
                        secureTextEntry={toggleOldPasswordShow}
                        onChangeText={(text) => setOldUserPassword(text)}
                    />
                    {toggleOldPasswordShow == false ? (
                        <TouchableOpacity onPress={() => setToggleOldPasswordShow(true)} style={{
                            position: 'absolute',
                            right: 0,
                            top: 15,
                            bottom: 30,
                            height: 30,
                            backgroundColor: 'transparent'
                        }}>
                            <Icon type="entypo" name="eye" size={20} color={COLORS.White} />
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity onPress={() => setToggleOldPasswordShow(false)} style={{
                            position: 'absolute',
                            right: 0,
                            top: 15,
                            bottom: 30,
                            height: 30,
                            backgroundColor: 'transparent'
                        }}>
                            <Icon type="entypo" name="eye-with-line" size={20} color={COLORS.White} />
                        </TouchableOpacity>
                    )}
                </View>
                <View style={{ justifyContent: 'center', }}>
                    <TextInput
                        style={styles.textInputCustom}
                        placeholder='New Password'
                        placeholderTextColor={COLORS.White}
                        secureTextEntry={toggleNewPasswordShow}
                        onChangeText={(text) => setNewPassword(text)}
                    />
                    {toggleNewPasswordShow == false ? (
                        <TouchableOpacity onPress={() => setToggleNewPasswordShow(true)} style={{
                            position: 'absolute',
                            right: 0,
                            top: 15,
                            bottom: 30,
                            height: 30,
                            backgroundColor: 'transparent'
                        }}>
                            <Icon type="entypo" name="eye" size={20} color={COLORS.White} />
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity onPress={() => setToggleNewPasswordShow(false)} style={{
                            position: 'absolute',
                            right: 0,
                            top: 15,
                            bottom: 30,
                            height: 30,
                            backgroundColor: 'transparent'
                        }}>
                            <Icon type="entypo" name="eye-with-line" size={20} color={COLORS.White} />
                        </TouchableOpacity>
                    )}
                </View>
                <View style={{ justifyContent: 'center', }}>
                    <TextInput
                        style={styles.textInputCustom}
                        placeholder='Confirm Password'
                        placeholderTextColor={COLORS.White}
                        secureTextEntry={toggleConfirmPasswordShow}
                        onChangeText={(text) => setConfirmPassword(text)}
                    />
                    {toggleConfirmPasswordShow == false ? (
                        <TouchableOpacity onPress={() => setToggleConfirmPasswordShow(true)} style={{
                            position: 'absolute',
                            right: 0,
                            top: 10,
                            bottom: 30,
                            height: 30,
                            backgroundColor: 'transparent'
                        }}>
                            <Icon type="entypo" name="eye" size={20} color={COLORS.White} />
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity onPress={() => setToggleConfirmPasswordShow(false)} style={{
                            position: 'absolute',
                            right: 0,
                            top: 15,
                            bottom: 30,
                            height: 30,
                            backgroundColor: 'transparent'
                        }}>
                            <Icon type="entypo" name="eye-with-line" size={20} color={COLORS.White} />
                        </TouchableOpacity>
                    )}
                </View>
                <TouchableOpacity
                    style={oldPassword == '' || newPassword == '' || confirmPassword == '' ? styles.disabledbtn : styles.btn}
                    disabled={oldPassword == '' || newPassword == '' || confirmPassword == '' ? true : false}
                    onPress={() => changePassword(localUser.token, localUser._id, oldPassword, newPassword)}
                >
                    {loading == false ? <Text style={styles.btnText}>Reset</Text> : <ActivityIndicator animating={true} color={COLORS.DeepBlue}></ActivityIndicator>}
                </TouchableOpacity>
            </Card>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.DeepBlue,
    },

    welcomeContainer: {
        marginTop: 50,
        textAlign: "justify"
    },

    text: {
        color: COLORS.White,
        marginLeft: 20,
        fontFamily: 'Trebuchet MS',
        fontWeight: 'bold'
    },

    subHeading: {
        marginLeft: 20,
        color: COLORS.White
    },

    textStyle: {
        color: COLORS.White,
    },

    btn: {
        marginTop: 30,
        width: 260,
        fontSize: 15,
        fontWeight: 'bold',
        height: 50,
        backgroundColor: COLORS.Cyan,
        borderRadius: 100,
        paddingTop: 5,
        paddingBottom: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },

    disabledbtn: {
        marginTop: 30,
        width: 260,
        height: 50,
        backgroundColor: COLORS.White,
        fontSize: 15,
        fontWeight: 'bold',
        borderRadius: 100,
        paddingTop: 5,
        paddingBottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },

    imgSize: {
        width: 200,
        height: 100,
        marginTop: 20,
        marginLeft: 100
    },

    card: {
        height: 450,
        width: 350,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: COLORS.DeepBlue,
        paddingTop: 50,
        paddingBottom: 40,
        paddingLeft: 50,
        paddingRight: 70,
        marginBottom: 50,
        marginTop: 100,
        marginLeft: 30,
    },

    firstwrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        marginLeft: 40
    },

    textInputCustom: {
        fontSize: 15,
        borderColor: COLORS.White,
        borderBottomWidth: 2,
        width: 250,
        marginBottom: 10,
        color: COLORS.White,
        fontWeight: "100",
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
})

export default ResetPasswordScreen