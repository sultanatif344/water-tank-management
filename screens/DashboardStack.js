import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { View, StyleSheet } from 'react-native';

import DashboardScreen from './Dashboard'
import AddNewTank from './AddNewTank'
import TankList from './TankList'
import WaterLevels from './WaterLevels'
import WaterStatus from './WaterStatus'
import MotorStatus from './MotorStatus'
import AdminPanel from './AdminPanel'
import NewUser from './NewUser'
import AddCylindricalTankPage from './AddCylindricalTank'
import AddCubicalTankPage from './AddCubicalTankPage'
import { COLORS } from '../components/theme';


const DashboardStack = () => {
    const HomeStack = createStackNavigator();
    return  (
        <HomeStack.Navigator>
                    <HomeStack.Screen name="Dashboard" component={DashboardScreen} options={{
                        title: ' ',
                        headerLeft: () => null,
                        headerStyle: {
                            height: 0,
                            shadowColor: 'transparent',
                            borderBottomWidth: 0,
                            elevation: 0,
                        }
                    }}
                    />

                    <HomeStack.Screen name="AddNewTank" component={AddNewTank} options={{
                        title: 'New Tank',
                        headerTintColor: '#ffffff',
                        headerStyle: {
                            backgroundColor: COLORS.DeepBlue,
                            height: 100,
                            borderBottomLeftRadius: 30,
                            borderBottomRightRadius: 20,
                            shadowColor: COLORS.Blue,
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 1.25,
                            shadowRadius: 10.84
                        }
                    }} />
                    <HomeStack.Screen name="TankList" component={TankList} options={{
                        title: 'Tank List',
                        headerTintColor: '#ffffff',
                        headerStyle: {
                            backgroundColor: COLORS.DeepBlue,
                            height: 100,
                            borderBottomLeftRadius: 30,
                            borderBottomRightRadius: 20,
                            shadowColor: COLORS.Blue,
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 1.25,
                            shadowRadius: 10.84
                        }
                    }} />

                    <HomeStack.Screen name="WaterLevels" component={WaterLevels} options={{
                        title: 'Water Levels',
                        headerTintColor: '#ffffff',
                        headerStyle: {
                            backgroundColor: COLORS.DeepBlue,
                            height: 100,
                            borderBottomLeftRadius: 30,
                            borderBottomRightRadius: 20,
                            shadowColor: COLORS.Blue,
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 1.25,
                            shadowRadius: 10.84
                        }
                    }} />
                    <HomeStack.Screen name="WaterStatus" component={WaterStatus} options={{
                        title: 'Water Status',
                        headerTintColor: '#ffffff',
                        headerStyle: {
                            backgroundColor: COLORS.DeepBlue,
                            height: 100,
                            borderBottomLeftRadius: 30,
                            borderBottomRightRadius: 20,
                            shadowColor: COLORS.Blue,
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 1.25,
                            shadowRadius: 10.84
                        }
                    }} />
                    <HomeStack.Screen name="MotorStatus" component={MotorStatus} options={{
                        title: 'Motor Status',
                        headerTintColor: '#ffffff',
                        headerStyle: {
                            backgroundColor: COLORS.DeepBlue,
                            height: 100,
                            borderBottomLeftRadius: 30,
                            borderBottomRightRadius: 20,
                            shadowColor: COLORS.Blue,
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 1.25,
                            shadowRadius: 10.84
                        }
                    }} />
                    <HomeStack.Screen name="AdminPanel" component={AdminPanel} options={{
                        title: 'Admin Panel',
                        headerTintColor: '#ffffff',
                        headerStyle: {
                            backgroundColor: COLORS.DeepBlue,
                            height: 130,
                            borderBottomLeftRadius: 30,
                            borderBottomRightRadius: 20,
                            shadowColor: COLORS.Blue,
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 1.25,
                            shadowRadius: 10.84,
                        },
                        headerTitleContainerStyle: { marginTop: 50 } 
                    }} />

                    <HomeStack.Screen name="NewUser" component={NewUser} options={{
                        title: 'New User',
                        headerTintColor: '#ffffff',
                        headerStyle: {
                            backgroundColor: COLORS.DeepBlue,
                            height: 80,
                            borderBottomLeftRadius: 30,
                            borderBottomRightRadius: 20,
                            shadowColor: COLORS.Blue,
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 1.25,
                            shadowRadius: 10.84
                        }
                    }} />

                    <HomeStack.Screen name="AddCubicalTankPage" component={AddCubicalTankPage} options={{
                        title: 'Cubical Tank',
                        headerTintColor: '#ffffff',
                        headerStyle: {
                            backgroundColor: COLORS.DeepBlue,
                            height: 80,
                            borderBottomLeftRadius: 30,
                            borderBottomRightRadius: 20,
                            shadowColor: COLORS.Blue,
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 1.25,
                            shadowRadius: 10.84
                        }
                    }}
                    />

                    <HomeStack.Screen name="AddCylindricalTankPage" component={AddCylindricalTankPage} options={{
                        title: 'Cylindrical Tank',
                        headerTintColor: '#ffffff',
                        headerStyle: {
                            backgroundColor: COLORS.DeepBlue,
                            height: 80,
                            borderBottomLeftRadius: 30,
                            borderBottomRightRadius: 20,
                            shadowColor: COLORS.Blue,
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 1.25,
                            shadowRadius: 10.84
                        }
                    }}
            />

            </HomeStack.Navigator>
        )
    
    }
            
export default DashboardStack;