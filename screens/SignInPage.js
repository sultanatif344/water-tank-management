import React, { Component, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  KeyboardAvoidingView,
  ActivityIndicator,
  Image,
  Button
} from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { COLORS } from '../components/theme';
import { Icon } from 'react-native-elements';
// import { Icon } from 'react-native-vector-icons';

import { authenticateUser, setUserLocally } from '../helpers/user';
import {
  LoginUserRequest,
  LoginUserSuccess,
  LoginUserFailure,
} from '../redux/user/userActions';
import { connect, useDispatch, useSelector } from 'react-redux';
import { presentFailureToast } from '../helpers/utility';
import Toast from 'react-native-toast-message';
const SignIn = ({ navigation }) => {
  const [user, setUserName] = useState('');
  const [password, setUserPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [togglePasswordShow, setTogglePasswordShow] = useState(true);
  const dispatch = useDispatch();
  const loginUser = async credentials => {
    dispatch(LoginUserRequest);
    setLoading(true);
    const userDetails = await authenticateUser(credentials);
    if (userDetails.success == true) {
      console.log('these are the user details', userDetails);
      setUserLocally(userDetails.user);
      dispatch(LoginUserSuccess(userDetails.user));
      setLoading(false);
      navigation.navigate('Dashboard');
    } else {
      dispatch(LoginUserFailure('Incorrect username or password'));
      presentFailureToast('Incorrect username or password');
      setLoading(false);
    }
  };
  return (
    <SafeAreaProvider style={{ flex: 1, backgroundColor: COLORS.Cyan }}>
      <Toast />
      {/* <Icon name="account-circle" size={200} color="white" style={{ marginTop: 30 }} /> */}
      <Image
        source={require('../assets/Images/user.png')}
        width="200"
        height="200"
        style={styles.imgStyle}
      />
      <TextInput
        placeholder="User Name"
        placeholderTextColor={COLORS.White}
        style={styles.textInputCustom}
        onChangeText={text => setUserName(text)}
      />
      <View style={{ justifyContent: 'center', }}>
        <TextInput
          placeholder="Password"
          placeholderTextColor={COLORS.White}
          secureTextEntry={togglePasswordShow}
          style={styles.textInputCustom}
          onChangeText={text => setUserPassword(text)}
        />
        {togglePasswordShow == false ? (
          <TouchableOpacity onPress={() => setTogglePasswordShow(true)} style={{
            position: 'absolute',
            right: 60,
            top: 55,
            bottom: 30,
            height:30,
            backgroundColor:'transparent'
          }}>
            <Icon type="entypo" name="eye" size={20} color={COLORS.White} />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={() => setTogglePasswordShow(false)} style={{
            position: 'absolute',
            right: 60,
            top: 55,
            bottom: 30,
            height:30,
            backgroundColor:'transparent'
          }}>
            <Icon type="entypo" name="eye-with-line" size={20} color={COLORS.White}  />
          </TouchableOpacity>
        )}
        {/* {route.params?.isEdit == true ? (
        <Icon type="entypo" name="eye" size={20} color="#000"/>
        }:(
        <Icon type="entypo" name="eye" size={20} color="#000"/>
        )
        <TextInput
          placeholder="Password"
          placeholderTextColor={COLORS.White}
          secureTextEntry={true}
          style={styles.textInputCustom}
          onChangeText={text => setUserPassword(text)}
        /> */}
      </View>
      <View>
        <TouchableOpacity
          style={user == '' || password == '' ? styles.disabledbtn : styles.btn}
          onPress={() => loginUser({ email: user, password: password })}
          disabled={user == '' || password == '' ? true : false}>
          {loading == false ? (
            <Text style={user == '' || password == '' ? styles.disabledbtnText : styles.btnText}>Login</Text>
          ) : (
            <ActivityIndicator
              animating={true}
              color={COLORS.Cyan}></ActivityIndicator>
          )}
        </TouchableOpacity>
        <Image source={require('../assets/Images/curve3.jpg')} style={{ position: 'relative', left: 1, bottom: 600, width: 450, height: 900, zIndex: -3 }} />
      </View>
    </SafeAreaProvider>
  );
};
// const mapStateToProps = state => {
//     return {
//       userData: state.users
//     }
//   }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.Cyan,
  },

  iconMargin: {
    marginTop: 100,
  },

  textInputCustom: {
    fontSize: 15,
    borderColor: COLORS.White,
    borderBottomWidth: 1,
    width: 300,
    height:50,
    backgroundColor: 'transparent',
    marginTop: 40,
    marginLeft: 60,
    color: COLORS.White,
    fontWeight: '100',
    
  },
  btn: {
    marginTop: 20,
    width: 162,
    height: 50,
    backgroundColor: COLORS.DeepBlue,
    borderRadius: 100,
    marginTop: 90,
    marginLeft: 120,
    paddingTop: 5,
    paddingBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },

  disabledbtn: {
    marginTop: 20,
    width: 162,
    height: 50,
    backgroundColor: COLORS.White,
    borderRadius: 100,
    marginTop: 50,
    marginLeft: 120,
    paddingTop: 5,
    paddingBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgStyle: {
    marginLeft: 150,
    marginTop: 100,
  },
  btnText: {
    fontSize: 25,
    color: COLORS.White,
    fontWeight: '100',
  },
  disabledbtnText: {
    fontSize: 25,
    color: COLORS.Black,
    fontWeight: '100',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },

  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});

export default SignIn;
