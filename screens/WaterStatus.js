/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { COLORS } from '../components/theme';
import { Text, Card, Icon, Divider } from 'react-native-elements';
import { waterLevelObj } from '../components/global';
import { getAllActiveTanksList } from '../helpers/tank';
import { getDataOnAdd } from '../helpers/firebase';
const WaterStatus = ({ navigation }) => {
  const [totalVolumeltr, setTotalVolumeltr] = useState(0);
  const [waterHeight, setWaterHeight] = useState(0);
  const [emptySpace, setEmptySpace] = useState(0);
  const [levelAccuracy, setLevelAccuracy] = useState(0);
  const setWaterInfoData = async () => {
    const activeTank = await getAllActiveTanksList();
    const firebaseDataSanpshot = await getDataOnAdd();
    const localWaterHeight = activeTank.height - firebaseDataSanpshot[0];
    setEmptySpace(firebaseDataSanpshot[0]);
    setWaterHeight(localWaterHeight);
    setLevelAccuracy((localWaterHeight / activeTank.height) * 100);
    if (activeTank.tankShape == 'Cubical') {
      const cubicalWaterVolume =
        activeTank.length * activeTank.width * localWaterHeight;
      const cubicalWaterVolumeInLitre = cubicalWaterVolume / 1000;
      setTotalVolumeltr(cubicalWaterVolumeInLitre);
    } else if (activeTank.tankShape == 'Cylinderical') {
      const radius = activeTank.diameter / 2;
      const cylindricalWaterVolume =
        3.142 * radius * localWaterHeight;
      const cylindricalWaterVolumeInLitre = cylindricalWaterVolume / 1000;
      setTotalVolumeltr(cylindricalWaterVolumeInLitre);
    }
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setInterval(()=>{
        setWaterInfoData()
      },5000)
    })
    return unsubscribe;
  }, [navigation]);

  return (
    <View style={styles.container}>
      <Card containerStyle={styles.card}>
        <View style={styles.dividerStyle}>
          <Text style={styles.textStyle}>
            Water Volume: {totalVolumeltr.toFixed(2)} ltr
          </Text>
        </View>

        <View style={styles.dividerStyle}>
          <Text style={styles.textStyle}>Water Height: {waterHeight.toFixed(2)} cm</Text>
        </View>

        <View style={styles.dividerStyle}>
          <Text style={styles.textStyle}>Empty Space: {emptySpace.toFixed(2)} cm</Text>
        </View>

        <View style={styles.dividerStyle}>
          <Text style={styles.textStyle}>
            Level Accuracy: {levelAccuracy.toFixed(2)}%
          </Text>
        </View>
        <View style={styles.dividerStyle}></View>
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  dividerStyle: {
    borderTopColor: COLORS.White,
    borderTopWidth: 2,
    paddingTop: 33,
    paddingBottom: 33,
  },

  textStyle: {
    fontSize: 20,
    textAlign: 'center',
  },
  card: {
    marginTop: 5,
    paddingTop: 30,
    width: 300,
    height: 480,
    backgroundColor: COLORS.Cyan,
    borderRadius: 10,
    shadowColor: COLORS.Black,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,

    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 1.25,
    shadowRadius: 10.84,
    elevation: 5,
  },
});

export default WaterStatus;
