import React, { useEffect, useState } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { getAllNotifications, getLocallyStoredUser, requestAllReviews } from '../helpers/user'
import { FAB, Rating } from 'react-native-elements';
import { COLORS } from '../components/theme';
import { Card } from 'react-native-elements';
import ReviewsListTvc from '../components/reviewsListtvc';
const ReviewsList = ({ navigation }) => {

    const [reviewsList, setReviews] = useState([]);
    const [user, setLocallyStoredUser] = useState({});

    const LocallyStoreUser = async () => {
        const localUser = JSON.parse(await getLocallyStoredUser());
        getAllReviewslist(localUser.token)
    }
    const getAllReviewslist = async (token) => {
        const reviews = await requestAllReviews(token);
        console.log("here are my reviews", reviews);
        setReviews(reviews)
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            // The screen is focused
            // Call any action
            
            LocallyStoreUser();
        });

        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);
    return (
        <View>
            <View>
                <FlatList
                    data={reviewsList}
                    renderItem={({ item }) =>
                        (<ReviewsListTvc item={item} />)
                    }
                    keyExtractor={(item, index) => item._id}
                />
                <View style={{position:'absolute', top:450, left:50}}> 
                <FAB title="+" style={{marginTop:150, marginLeft:250}} color={COLORS.DeepBlue} size='small' onPress={() => navigation.navigate('Feedback')} />
            </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        fontSize: 20,
        color: COLORS.White
    },
    editStyle: {
        fontSize: 15,
        color: COLORS.White,
        width: 162,
        marginTop: 5,
        height: 50,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 100,
        paddingTop: 10,
        paddingBottom: 5,
        textAlign: 'center'
    },
    deleteStyle: {
        fontSize: 15,
        color: COLORS.White,
        backgroundColor: COLORS.Red,
        width: 162,
        height: 50,
        marginTop: 5,
        borderRadius: 100,
        paddingTop: 10,
        paddingBottom: 5,
        textAlign: 'center'
    },
    card: {
        flexDirection: 'row',
        flexWrap: 'nowrap',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: COLORS.Black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 2,
        shadowRadius: 10.84,
        elevation: 5,
        backgroundColor: COLORS.Cyan,
        paddingVertical: 25
    },
    BoxText: {
        color: COLORS.White,
        marginTop: 5,
        marginLeft: 10,
        fontSize: 12
    },

    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    wrapper: {
        display: 'flex',
        marginRight: 10
    },

    secondwrapper: {
        display: 'flex',
        flexDirection: 'row',

    },

    containerwrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})
export default ReviewsList;