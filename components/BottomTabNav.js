import React, { Component, useState } from 'react'
import {
    StyleSheet,
    View,
    TouchableOpacity,
    StatusBar,
    BackHandler
} from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Text, Icon } from 'react-native-elements';
import { COLORS } from './theme';
import DashboardStack from '../screens/DashboardStack'
import DashboardScreen from '../screens/Dashboard';
import Settings from '../screens/settings';
import { clearAllLocalItems } from '../helpers/utility';
import NotificationHistory from '../screens/NotificationHistory';
import { CommonActions } from '@react-navigation/native';
const Tab = createBottomTabNavigator();
const TabBarNav = ({ navigation }) => {
    return (
        <Tab.Navigator
            screenOptions={{
                showLabel: false,
                tabBarInactiveBackgroundColor: COLORS.DeepBlue,
                tabBarActiveBackgroundColor: COLORS.DeepBlue,
                headerShadowVisible: true,
                headerStyle: {
                    backgroundColor: COLORS.DeepBlue,
                    height: 70
                },
                headerTitleContainerStyle: {
                    padding: 10,
                },
                headerTintColor: "#FFFFFF",
                tabBarItemStyle: {
                    paddingTop: 4,
                },
                tabBarStyle: {
                    height: 70,
                    borderTopLeftRadius: 21,
                    borderTopRightRadius: 21,
                    borderColor: 'transparent',
                    overflow: 'hidden',
                    width: 410,
                    position: 'absolute',
                    left: 1,
                    right: 50,
                    bottom: 1,
                },
            }}
        >
            <Tab.Screen name="Dashboard" component={DashboardScreen} options={{
                tabBarIcon: ({ focused }) => (
                    <View>
                        <Icon
                            name='home'
                            type='font-awesome'
                            color={focused ? COLORS.Cyan : COLORS.White}
                            size={30}
                        />
                    </View>
                ),
                title: ' ',
                headerLeft: () => null,
                headerStyle: {
                    height: 0,
                    shadowColor: 'transparent',
                    borderBottomWidth: 0,
                    elevation: 0,
                },
            }}
            />
            <Tab.Screen name="Notifications" component={NotificationHistory} options={{
                tabBarIcon: ({ focused }) => (
                    <View>
                        <Icon
                            name='bell'
                            type='font-awesome'
                            color={focused ? COLORS.Cyan : COLORS.White}
                            size={30}
                            style={{
                                marginBottom: 10
                            }}
                        />
                    </View>
                ),
                headerStyle:{
                    backgroundColor: COLORS.DeepBlue,
                    height: 100,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                },
                tabBarLabelStyle: {
                    display: 'none',
                },
            }}
            />
            <Tab.Screen name="Settings" component={Settings} options={{
                tabBarIcon: ({ focused }) => (
                    <View>
                        <Icon
                            name='gear'
                            type='font-awesome'
                            color={focused ? COLORS.Cyan : COLORS.White}
                            size={30}
                            style={{
                                marginBottom: 10
                            }}
                        />
                    </View>
                ),
                headerStyle:{
                    backgroundColor: COLORS.DeepBlue,
                    height: 100,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 20,
                    shadowColor: COLORS.Blue,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 1.25,
                    shadowRadius: 10.84
                },
                tabBarLabelStyle: {
                    display: 'none',
                },
            }}

            />
            <Tab.Screen name="Logout" component={DashboardScreen} options={{
                tabBarIcon: ({ focused }) => (
                    <View>
                        <Icon
                            name='sign-out'
                            type='font-awesome'
                            color={focused ? COLORS.Cyan : COLORS.White}
                            size={30}
                            onPress={() => {
                                clearAllLocalItems();
                                navigation.reset({
                                    index: 0,
                                    routes: [
                                        {
                                            name: 'SignIn'
                                        }
                                    ]
                                })
                            }}
                        />
                    </View>
                ),
                title: ' ',
                headerLeft: () => null,
                headerStyle: {
                    height: 0,
                    shadowColor: 'transparent',
                    borderBottomWidth: 0,
                    elevation: 0,
                }
            }}

            />
        </Tab.Navigator>
    )
}

const styles = StyleSheet.create({
    container: {
        borderTopLeftRadius: 80

    }
})



export default TabBarNav
