import React, { Component } from 'react'
import {
    AppRegistry,
    StyleSheet,
    View,
    Button,
    TouchableOpacity
} from 'react-native'



import { Text} from 'react-native-elements';

const Login = () => {

    return (
        <View style={styles.container}>
            <Text h1 style={styles.text}>Welcome!</Text>
            <Text h5 style={styles.subHeading}>Nice To See You Again.</Text>
            <TouchableOpacity
                style={styles.btn}
                onPress=""
            >
                <Text style={styles.btnText}>Login</Text>
            </TouchableOpacity>
        </View>
    )   
}


const styles = StyleSheet.create({
    container: {
        marginTop: 150,
        textAlign: 'left'
    },

    text: {
        color: '#ffffff',
        marginLeft: 20,
        fontFamily: 'Trebuchet MS',
        fontWeight: 'bold'
    },

    subHeading: {
        marginLeft: 20,
        color: '#818181'
    },

    btn: {
        marginTop: 20,
        width: 162,
        height: 50,
        backgroundColor: '#53B9AB',
        borderRadius: 100,
        marginLeft: 25,
        paddingTop: 5,
        paddingBottom: 5,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    btnText: {
        fontSize: 25,
        color: '#03102B'
    }
})


export default Login;
