import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList, Text, TouchableOpacity } from 'react-native';
import {getTanks, deleteTankById} from '../helpers/tank'
import { useSelector, useDispatch } from 'react-redux';
import { GetTankRequest, GetTankSuccess, GetTankFailure, DeleteTankRequest, DeleteTankSuccess, DeleteTankFailure } from '../redux/tank/tankActions';
import { ListItem } from 'react-native-elements/dist/list/ListItem';
import {COLORS} from './theme';
import { Card } from 'react-native-elements';
import { DeleteUserRequest, DeleteUserSuccess, DeleteUserFailure } from '../redux/user/userActions';
import { deleteUserById } from '../helpers/user';

const NotificationHistoryListTvc = ({notificationParam}) => {
    const [notification, setNotification] = useState({notificationType:"",description:""});
    const deleteUser = (token,id) =>{
        return async ()=>{
            dispatch(DeleteUserRequest);
            const res = await deleteUserById(token,id);
            if(res.success == true){
                dispatch(DeleteUserSuccess)
                initialize();
            }
            else{
                dispatch(DeleteUserFailure);
            }
        }
    }
    const initialize = () =>{
        // console.log("got user",{user})
        // setUsers(user);
        console.log("this is my notification param", notificationParam)
        setNotification(notificationParam);
    }
    useEffect(()=>{
        initialize();
    },[])
    return (
        <View style={styles.container}>
            <Card containerStyle={styles.card}>
            <View style={styles.containerwrapper}>
                        <View style={styles.wrapper}>
                            <View style={styles.secondwrapper}>
                                <Text style={styles.BoxText}>Notification Type: {notification.notificationType}</Text>
                            </View>

                            <View style={styles.secondwrapper}>
                                <Text style={styles.BoxText}>Description: {notification.description}</Text>
                            </View>
                        </View>
                        <View style={styles.wrapper}>
                            {/* <View style={styles.secondwrapper}>
                                <TouchableOpacity onPress={deleteUser(userSelector.users.token,"1234567")}><Text style={styles.deleteStyle}>Delete Tank</Text></TouchableOpacity>
                            </View> */}
                        </View>
                    </View>
            </Card>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    textStyle:{
        fontSize:20,
        color:COLORS.White
    },
    editStyle:{
        fontSize:15,
        color:COLORS.White,
        width: 162,
        marginTop:5,
        height: 50,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 100,
        paddingTop: 10,
        paddingBottom: 5,
        textAlign:'center'
    },
    deleteStyle:{
        fontSize:15,
        color:COLORS.White,
        backgroundColor:COLORS.Red,
        width: 162,
        height: 50,
        marginTop:5,
        borderRadius: 100,
        paddingTop: 10,
        paddingBottom: 5,
        textAlign:'center'
    },
    card: {
        flexDirection:'row',
        flexWrap:'nowrap',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: COLORS.Black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 2,
        shadowRadius: 10.84,
        elevation: 5,
        backgroundColor:COLORS.Cyan,
        paddingVertical:25
    },
    BoxText: {
        color: COLORS.White,
        marginTop:5,
        marginLeft: 10,
        fontSize: 12
    },

    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    wrapper: {
        display: 'flex',
        marginRight: 10
    },

    secondwrapper: {
        display: 'flex',
        flexDirection: 'row',
        
    },

    containerwrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})

export default NotificationHistoryListTvc;