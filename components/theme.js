export const COLORS = {
    "White": "#FFFFFF",
    "DeepBlue": "#03102B",
    "Black": "#00000029",
    "Cyan": "#53B9AB",
    "BluishBlack": "#02282E",
    "Blue": "#123C92",
    "Grey": "#818181",
    "Red":"#FF0000"
}