import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList, Text, TouchableOpacity } from 'react-native';
import { getTanks, deleteTankById } from '../helpers/tank'
import { useSelector, useDispatch } from 'react-redux';
import { GetTankRequest, GetTankSuccess, GetTankFailure, DeleteTankRequest, DeleteTankSuccess, DeleteTankFailure } from '../redux/tank/tankActions';
import { ListItem } from 'react-native-elements/dist/list/ListItem';
import { COLORS } from './theme';
import { Card, Icon } from 'react-native-elements';
import { DeleteUserRequest, DeleteUserSuccess, DeleteUserFailure } from '../redux/user/userActions';
import { deleteUserById } from '../helpers/user';

const UserListTvc = ({ user, navigation }) => {
    const userSelector = useSelector((state) => state.user);
    const dispatch = useDispatch();
    const [users, setUsers] = useState(() => []);
    const deleteUser = (token, id) => {
        return async () => {
            dispatch(DeleteUserRequest);
            const res = await deleteUserById(token, id);
            if (res.success == true) {
                dispatch(DeleteUserSuccess)
            }
            else {
                dispatch(DeleteUserFailure);
            }
        }
    }
    const initialize = () => {
        console.log("got user", { user })
        setUsers(user);
    }
    useEffect(() => {
        initialize();
    }, [setUsers, user])
    return (
        <View style={styles.container}>
            <Card containerStyle={styles.card}>
                <View style={styles.containerwrapper}>
                    <View style={styles.wrapper}>
                        <View style={styles.secondwrapper}>
                            <Text style={styles.BoxText}>Username: {user.username}</Text>
                        </View>

                        <View style={styles.secondwrapper}>
                            <Text style={styles.BoxText}>Email: {user.email}</Text>
                        </View>

                        <View style={styles.secondwrapper}>
                            <Text style={styles.BoxText}>User Type: {user.userType}</Text>
                        </View>
                    </View>
                    <View style={styles.wrapper}>
                        <View style={styles.secondwrapper}>
                            <TouchableOpacity onPress={() => navigation.navigate("NewUser", {
                                item: user,
                                isEdit: true
                            })}><Text style={styles.deleteStyle}><Icon name='edit'
                                type='font-awesome'
                                color={COLORS.White}
                                size={20}
                            />   Edit</Text></TouchableOpacity>
                        </View>
                        <View style={styles.secondwrapper}>
                            <TouchableOpacity onPress={() => deleteUser(userSelector.users.token, user._id)}><Text style={styles.deleteStyle}><Icon name='trash'
                                type='font-awesome'
                                color={COLORS.White}
                                size={20}
                            />   Delete</Text></TouchableOpacity>
                        </View>
                    </View>

                    {/* <View style={styles.secondwrapper}>
                                <TouchableOpacity onPress={()=>navigation.navigate("NewUser",{
                                    item:user,
                                    isEdit:true
                                })}>
                                    <Text style={styles.editStyle}>Edit User</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.secondwrapper}>
                                <TouchableOpacity onPress={deleteUser(userSelector.users.token,user._id)}><Text style={styles.deleteStyle}>Delete Tank</Text></TouchableOpacity>
                            </View> */}
                </View>
            </Card >
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        fontSize: 20,
        color: COLORS.White
    },
    editStyle: {
        fontSize: 13,
        color: COLORS.White,
        width: 130,
        marginTop: 5,
        marginLeft: 25,
        marginBottom: 5,
        height: 40,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 100,
        paddingTop: 5,
        paddingBottom: 10,
        textAlign: 'center'
    },
    deleteStyle: {
        fontSize: 13,
        color: COLORS.White,
        width: 130,
        marginTop: 5,
        marginLeft: 25,
        marginBottom: 5,
        height: 40,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 100,
        paddingTop: 5,
        paddingLeft: 15,
        paddingBottom: 10,
        textAlign: 'center'
    },
    card: {
        flexDirection: 'row',
        flexWrap: 'nowrap',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: COLORS.Black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 2,
        shadowRadius: 10.84,
        elevation: 5,
        backgroundColor: COLORS.Cyan,
        paddingVertical: 25
    },
    BoxText: {
        color: COLORS.White,
        marginTop: 5,
        marginLeft: 10,
        fontSize: 12
    },

    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    wrapper: {
        display: 'flex',
        marginRight: 10
    },

    secondwrapper: {
        display: 'flex',
        flexDirection: 'row',

    },

    containerwrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})

export default UserListTvc