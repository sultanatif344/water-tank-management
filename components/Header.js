import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    TouchableOpacity,
    StatusBar,
    Image
} from 'react-native'

import { Text, Icon } from 'react-native-elements';
import { COLORS } from './theme';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { waterLevelObj } from '../components/global'
const DashboardHeader = ({ waterFilled }) => {
    console.log("amount filled", waterFilled)
    return (
        <View style={styles.container}>
            <Image source={require('../assets/logo.png')} style={styles.imgSize} />
            <View style={styles.gaugeStyle}>
                <AnimatedCircularProgress
                    rotation={395}
                    size={130}
                    width={5}
                    fill={waterFilled ? waterFilled : 0}
                    tintColor={COLORS.Cyan}
                    onAnimationComplete={() => console.log('onAnimationComplete')}
                    backgroundColor={COLORS.Grey}
                >
                    {
                        (fill) => (
                            [
                                <Text style={styles.guageTextPercentStyle}>
                                    {Math.round(fill)}<Text style={styles.guageTextStyle}> %</Text>
                                </Text>,
                                <Text style={styles.guageTextStyle}>
                                    Water
                                </Text>
                            ]
                        )

                    }
                </AnimatedCircularProgress>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: COLORS.DeepBlue,
        height: 280,
        marginTop: 0,
        elevation: 1,
        textAlign: "left",
        shadowColor: COLORS.Blue,
        borderBottomLeftRadius: 35,
        borderBottomRightRadius: 35,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 1.25,
        shadowRadius: 10.84,
        elevation: 5
    },

    layout: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 45,
        marginRight: 10,
    },

    menuIcon: {
        marginLeft: 10
    },

    refreshIcon: {
        marginLeft: 50,
        marginTop: 70
    },

    gaugeStyle: {
        flex: 1,
        alignItems: 'center',
        marginBottom: 80,
        lineHeight: 20
    },

    guageTextStyle: {
        color: COLORS.White,
        fontWeight: 'bold',
        fontSize: 10,
    },

    guageTextPercentStyle: {
        color: COLORS.White,
        fontWeight: 'bold',
        fontSize: 40,
    },
    imgSize: {
        width: 180,
        height: 100,
        marginTop: 5,
        marginLeft: 120,
        marginBottom: 10
    },
})


export default DashboardHeader
