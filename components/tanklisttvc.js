import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList, Text, TouchableOpacity } from 'react-native';
import { getTanks, deleteTankById, sendPayloadToToggleTank } from '../helpers/tank'
import { useSelector, useDispatch } from 'react-redux';
import { GetTankRequest, GetTankSuccess, GetTankFailure, DeleteTankRequest, DeleteTankSuccess, DeleteTankFailure } from '../redux/tank/tankActions';
import { ListItem } from 'react-native-elements/dist/list/ListItem';
import { COLORS } from './theme';
import { Card, Icon } from 'react-native-elements';
import Toast from 'react-native-toast-message';
import { presentSuccessToast, removeLocallyStoredItem, setItemLocally } from '../helpers/utility';
import { getLocallyStoredUser } from '../helpers/user';

const TankListTvc = ({tankList,setTankList, navigation, tank, previousActiveTank }) => {
    const userSelector = useSelector((state) => state.user);
    const dispatch = useDispatch();
    const [tanks, setTanks] = useState(() => []);
    const [previousTank, setPreviousActivatedTank] = useState(null);
    const [tankToToggle, setTankToToggle] = useState(null);
    const [loading, setLoading] = useState(false);
    const [localUser, setLocalUser] = useState({});
    const deleteTank = async (id) => {
            dispatch(DeleteTankRequest);
            const res = await deleteTankById(localUser.token, id);
            if (res.success == true) {
                const filteredTankList = tankList.filter((e)=> e._id != id);
                setTankList(filteredTankList);
                initialize();
            }
            else {
                dispatch(DeleteTankFailure);
            }
    }
    const initialize = () => {
        setTanks(tank);
        setPreviousActivatedTank(previousActiveTank);
    }

    // const toggleTank = async (previouslyAddedTankId, tankId, toggleValue) => {
    //     setLoading(true);
    //     const previousTankId = previouslyAddedTankId != null ? previouslyAddedTankId : null;
    //     const res = await sendPayloadToToggleTank(localUser.token, previousTankId, tankId, toggleValue);

    //     if (res.success == true) {
    //         setLoading(false);
    //         if (toggleValue == true) {
    //             setItemLocally(JSON.stringify(tank), 'activatedTank');
    //         }
    //         else {
    //             removeLocallyStoredItem('activatedTank')
    //         }
    //     }
    // }

    const userSetter = async () => {
        const user = await getLocallyStoredUser();
        const locallyStoredUser = {};
        if (user != null) {
            console.log("this is the local", user)
            const storedUser = JSON.parse(user);
            setLocalUser(storedUser);
        }
    }
    useEffect(() => {
        userSetter();
        initialize();
    }, [setTanks, tank])
    return (
        <View style={styles.container}>
            <Card containerStyle={styles.card}>
                <View style={styles.containerwrapper}>
                    <View style={styles.wrapper}>
                        <View style={styles.secondwrapper}>
                            <Text style={styles.BoxText}>Tank Shape: {tanks?.tankShape}</Text>
                        </View>

                        <View style={styles.secondwrapper}>
                            <Text style={styles.BoxText}>Length: {tanks?.length}</Text>
                        </View>

                        <View style={styles.secondwrapper}>
                            <Text style={styles.BoxText}>Height: {tanks?.height}</Text>
                        </View>
                        <View style={styles.secondwrapper}>
                            <Text style={styles.BoxText}>Diameter: {tanks?.diameter}</Text>
                        </View>
                        <View style={styles.secondwrapper}>
                            <Text style={styles.BoxText}>Width: {tanks.width}</Text>
                        </View>
                    </View>
                    <View style={styles.wrapper}>
                        <View style={styles.secondwrapper}>
                            <TouchableOpacity onPress={() => tanks.tankShape == "Cubical" ? navigation.navigate("AddCubicalTankPage", {
                                item: tanks,
                                isEdit: true
                            }) : navigation.navigate("AddCylindricalTankPage", {
                                item: tanks,
                                isEdit: true
                            })}>
                                <Text style={styles.editStyle}> <Icon name='edit'
                                    type='font-awesome'
                                    color={COLORS.White}
                                    size={20}
                                /> Edit Tank</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.secondwrapper}>
                            <TouchableOpacity onPress={()=>deleteTank(tank._id)}><Text style={styles.deleteStyle}><Icon name='trash'
                                type='font-awesome'
                                color={COLORS.White}
                                size={20}
                            /> Delete Tank</Text></TouchableOpacity>
                        </View>

                        <View style={styles.secondwrapper}>
                            <TouchableOpacity onPress={() => tank.activated == false ? toggleTank(previousTank ? previousTank?._id : null, tank._id, true) : toggleTank(previousTank ? previousTank?._id : null, tank._id, false)}>
                                {
                                    tank.activated == false ?
                                        <Text style={styles.deleteStyle}><Icon name='power-off'
                                            type='font-awesome'
                                            color={COLORS.White}
                                            size={20}
                                        /> Activate Tank</Text>
                                        :
                                        <Text style={styles.deleteStyle}><Icon name='power-off'
                                            type='font-awesome'
                                            color={COLORS.White}
                                            size={20}
                                        /> Deactivate Tank</Text>
                                }
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Card>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        fontSize: 20,
        color: COLORS.White
    },
    editStyle: {
        fontSize: 13,
        color: COLORS.White,
        width: 152,
        marginTop: 5,
        marginLeft: 25,
        marginBottom: 5,
        height: 40,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 100,
        paddingTop: 5,
        paddingBottom: 10,
        textAlign: 'center'
    },
    deleteStyle: {
        fontSize: 13,
        color: COLORS.White,
        width: 152,
        marginTop: 5,
        marginLeft: 25,
        marginBottom: 5,
        height: 40,
        backgroundColor: COLORS.DeepBlue,
        borderRadius: 100,
        paddingTop: 5,
        paddingLeft: 15,
        paddingBottom: 10,
        textAlign: 'center'
    },
    card: {
        flexDirection: 'row',
        flexWrap: 'nowrap',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: COLORS.Black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 2,
        shadowRadius: 10.84,
        elevation: 5,
        backgroundColor: COLORS.Cyan,
        paddingVertical: 25
    },
    BoxText: {
        color: COLORS.White,
        marginTop: 5,
        marginLeft: 10,
        fontSize: 13
    },

    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    wrapper: {
        display: 'flex',
        marginRight: 10
    },

    secondwrapper: {
        display: 'flex',
        flexDirection: 'row'
    },

    containerwrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})

export default TankListTvc