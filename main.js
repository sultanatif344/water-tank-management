import React from 'react'
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
} from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { MainRoute } from './routes/routes';
const Main = ({ route }) => {
  console.log("this is the route", route)
  return (
    <NavigationContainer>
      <MainRoute route={route} />
    </NavigationContainer>
  );
}

export default Main;