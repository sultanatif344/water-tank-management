import React, { Component, useEffect, useState } from 'react'
import Main from './main';
import { Provider } from 'react-redux';
import store from './redux/store';
import messaging from "@react-native-firebase/messaging";
import { addNotificationHistory, getLocallyStoredUser, updateFcmToken } from './helpers/user';
import { getLocallyStoredItem, setItemLocally } from './helpers/utility';
import { Alert, View, Text } from 'react-native';
import { onRemoteMessageReceived } from './helpers/fcmHelper';
import { getAllActiveTanksList, getTankFilledValueInPercentage, sendCustomNotificationPayload } from './helpers/tank';
import NetInfo from '@react-native-community/netinfo';
const App = () => {
  const [user, setUser] = useState(() => null);
  const [isLoggedIn, setIsLoggedIn] = useState(() => null);
  const [isOnline, setIsOnline] = useState(null);
  const checkUser = async () => {
    const locallyStoredUser = await getLocallyStoredUser();
    if (locallyStoredUser != null) {
      // console.log("this is the local", locallyStoredUser)
      const storedUser = JSON.parse(locallyStoredUser);
      setUser(storedUser);
      setIsLoggedIn(true)
    }
    else {
      setIsLoggedIn(false);
    }
  }
  const getTankLevelInPercentage = () => {
    return new Promise(async resolve => {
      let activeTank = await getAllActiveTanksList();
      let tankFilledInfo = await getTankFilledValueInPercentage(activeTank.height);
      resolve(tankFilledInfo)
    })
  }
  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      onRemoteMessageReceived(remoteMessage);
    });

    if (isLoggedIn == true) {
      getTankLevelInPercentage().then(async res => {
        if (res == 25 || res == 50 || res == 75 || res >= 98) {
          const locallyStoredToken = await getLocallyStoredItem('fcmToken');
          var message = res < 98 ? `The current level Of your water tank is ${res}%` : `Your water tank is full please switch off your motor`;
          const payload = {
            'body': message,
            'title': 'Water Level Status',
            'fcmToken': locallyStoredToken
          }

          const result = await sendCustomNotificationPayload(payload);
          console.log("this is the result", result)
          if (result && res == 25) {
            const historyRes = await addNotificationHistory(user.token, { notificationType: 'info', description: payload.body });
          }
          if (result && res == 50) {
            const historyRes = await addNotificationHistory(user.token, { notificationType: 'normal', description: payload.body });
          }
          if (result && res == 75) {
            const historyRes = await addNotificationHistory(user.token, { notificationType: 'warning', description: payload.body });
          }
          if (result && res >= 98) {
            const historyRes = await addNotificationHistory(user.token, { notificationType: 'danger', description: payload.body });
          }
        }
      })
    }
    return unsubscribe;
  }, []);




  useEffect(() => {
    checkUser().then(() => {

      messaging()
        .getToken()

        .then(async (token) => {

          const localuser = JSON.parse(await getLocallyStoredUser())
          if (localuser != null) {
            console.log("this is th user", localuser);
            const result = await updateFcmToken(localuser, token);
            console.log(token)
            // localStorage.setItem("fcmToken", token);
            setItemLocally(token, 'fcmToken').then(result => {
              console.log("works", result)
            })

            const unsubscribe = messaging().onTokenRefresh(async (token) => {
              console.log("FCM TOKEN", token);
              // CALL API
              console.log("fcm token", token);
              const result = await updateFcmToken(user, token);
            });

            return unsubscribe;
          }

          else {
            setItemLocally(token, 'fcmToken').then(result => {
              console.log("it worked?", result)
            })
          }
          // Call API
        });
    })
  }, []);

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      if (state.isConnected == true) {
        setIsOnline(true);
      }
      else {
        setIsOnline(false);
      }
    });

    // Unsubscribe
    return () => unsubscribe();
  }, [])

  if (isLoggedIn != null) {
    return (
      <Provider store={store}>
        {
          isOnline == false ?(
        <View style={{ width: 'auto', height: 'auto', backgroundColor: 'red' }}>
          <Text style={{ color: 'white', textAlign: 'center' }}>No Internet Connection</Text>
        </View>
        ):(
          <View></View>
        )}
        <Main route={isLoggedIn == true ? 'Dashboard' : 'Welcome'} />
      </Provider>
    );
  }
  else {
    return null
  }
}
export default App;
